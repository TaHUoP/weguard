<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $style
 * @var $categories [N]['category_id']
 * @var $categories [N]['type']
 * Shortcode class
 * @var $this WPBakeryShortCode_Section_Categories
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$atts = $this->convertAttributesToArray($atts);
extract($atts);
$categories = (array)vc_param_group_parse_atts($categories);
if ($style == 'A') { ?>
    <ul class="b-advantages-2">
        <?php foreach ($categories as $category) {
            $font_icon = $category['icon_' . $category['type']];
            $category = get_category($category['category_id']);
            ?>
            <li class="b-advantages-2__item"><i class="b-advantages-2__icon <?php echo $font_icon; ?>"></i>
                <div class="b-advantages-2__inner">
                    <div class="b-advantages-2__name"><?php echo $category->name; ?></div>
                    <div class="b-advantages-2__text"><?php echo $category->category_description; ?></div>
                    <a href="<?php echo get_category_link($category->term_id); ?>" class="b-advantages-2__link btn-link">read details</a>
                </div>
            </li>
        <?php } ?>
    </ul>
<?php } elseif ($style == 'B') {
    for ($i = 0; $i < count($categories); $i = $i + 5) { ?>
        <ul class="b-advantages-4">
            <?php for ($k = 0; $k < 5; $k++) {
                $font_icon = $categories[$i + $k]['icon_' . $categories[$i + $k]['type']];
                $category = get_category($categories[$i + $k]['category_id']);
                if (isset($categories[$i + $k])) {
                    ?>
                    <li class="b-advantages-4__item">
                        <div class="b-advantages-4__inner">
                            <i class="b-advantages-4__icon <?php echo $font_icon; ?>"></i>
                            <div class="b-advantages-4__title"><?php echo $category->name; ?></div>
                            <i class="b-advantages-4__decor pe-7s-angle-down"></i>
                            <div class="b-advantages-4__text"><?php echo $category->category_description; ?></div>
                        </div>
                    </li>
                <?php } ?>
            <?php } ?>
        </ul>
    <?php } ?>
<?php } ?>