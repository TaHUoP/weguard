<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $title
 * @var $logo
 * @var $info_items [N]['title']
 * @var $info_items [N]['text']
 * @var $info_items [N]['type']
 * Shortcode class
 * @var $this WPBakeryShortCode_Block_Info
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$atts = $this->convertAttributesToArray($atts);
extract($atts);
$info_items = (array)vc_param_group_parse_atts($info_items);

$img_id = preg_replace('/[^\d]/', '', $logo);
$img_meta_array = weguard_wp_get_attachment($img_id);
?>

<section class="b-type-d section-bg jarallax section-table__inner">
    <div class="section__inner"><img src="<?php echo $img_meta_array['src'];?>" alt="<?php echo $img_meta_array['alt'];?>" class="img-responsive"/>
        <h2 class="b-type-d__title ui-title-inner-1"><?php echo $title;?></h2>
        <dl class="b-type-d__list-desription list-desription">
            <?php foreach ($info_items as $info_item) {
                $font_icon = $info_item['icon_' . $info_item['type']];
                ?>
                <dt><i class="icon <?php echo $font_icon;?>"></i><?php echo $info_item['title'];?></dt>
                <dd><?php echo $info_item['text'];?></dd>
            <?php } ?>
        </dl>
    </div>
</section>