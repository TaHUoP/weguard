<form role="search" method="get" class="search-form form-search-global" action="<?php echo home_url( '/' ); ?>">
    <label>
        <input type="search" class="search-field form-search-global__input" placeholder="Type to search" value="<?php echo get_search_query() ?>" name="s" title="" />
    </label>
<!--    <input type="submit" class="search-submit form-search-global__btn" value="--><?php //echo __('Найти', 'coral');?><!--" />-->
    <button class="form-search-global__btn"><i class="icon fa fa-search"></i></button>

    <div class="form-search-global__note">Begin typing your search above and press return to search.</div>
</form>