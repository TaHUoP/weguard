<?php
/**
 * Template part for displaying posts.
 */

?>

<article class="b-post b-post-3 clearfix">
    <div class="entry-media">
        <a href="<?php the_post_thumbnail_url(); ?>" class="js-zoom-images">
            <?php the_post_thumbnail(); ?>
        </a>
    </div>
    <div class="entry-main">
        <div class="entry-header">
            <div class="entry-date"><?php echo get_the_date('j M Y'); ?></div>
            <h2 class="entry-title entry-title_spacing ui-title-inner"><?php echo get_the_title(); ?></h2>
            <div class="entry-meta">
                <span class="entry-meta__item">
                    <i class="entry-meta__icon pe-7s-user"></i>By <?php echo get_the_author_link(); ?>
                </span>
                <span class="entry-meta__item">
                    <i class="entry-meta__icon pe-7s-comment"></i>Comments:<a href="<?php echo get_the_permalink() . '#comments';?>" class="entry-meta__link"> <?php echo get_comments_number(); ?></a>
                </span>
            </div>
        </div>
        <div class="entry-content">
            <p><?php echo get_the_excerpt(); ?></p>
        </div>
        <div class="entry-footer">
            <a href="<?php echo get_the_permalink();?>" class="btn btn-sm">read more</a>
            <a href="<?php echo get_the_permalink();?>" class="entry-footer__link"><i class="icon pe-7s-share"></i> Share This</a>
        </div>
    </div>
</article>
<!-- end post-->