<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $icons[N]['type']
 * Shortcode class
 * @var $this WPBakeryShortCode_Block_Footer_Social_Icons
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$atts = $this->convertAttributesToArray($atts);
extract($atts);
$icons = (array)vc_param_group_parse_atts($icons)

?>
<ul class="social-net list-inline">
    <?php foreach ($icons as $icon){
        $font_icon = $icon['icon_' . $icon['type']];
        $icon_link = vc_build_link($icon['link']);
        ?>
        <li class="social-net__item">
            <a href="<?php echo $icon_link['url'];?>" class="social-net__link"><i class="icon fa <?php echo $font_icon;?>"></i></a>
        </li>
    <?php } ?>
</ul>