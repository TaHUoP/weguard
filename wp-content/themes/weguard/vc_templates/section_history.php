<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $button_text
 * @var $history_items [N]['date']
 * @var $history_items [N]['title']
 * @var $history_items [N]['text']
 * @var $history_items [N]['type']
 * Shortcode class
 * @var $this WPBakeryShortCode_Section_History
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$atts = $this->convertAttributesToArray($atts);
extract($atts);
$history_items = (array)vc_param_group_parse_atts($history_items);

$widget_id = mt_rand(1000,9999);
?>

<script>

    jQuery(document).ready(function ($) {

        $("#show_more_history_<?php echo $widget_id;?>").on("click", function () {

            var hiddenContent = $("#history_<?php echo $widget_id;?> .js-scroll-content:hidden").first();

            console.log(hiddenContent);

            hiddenContent.show();

            if ($("#history_<?php echo $widget_id;?> .js-scroll-content:hidden").first().length == 0) {
                $("#show_more_history_<?php echo $widget_id;?>").hide();
            }

            hiddenContent.addClass("animated");
            hiddenContent.addClass("animation-done");
            hiddenContent.addClass("bounceInUp");

        });
    });

</script>

<div class="text-center" id="history_<?php echo $widget_id;?>">
    <?php for ($i = 0; $i < count($history_items); $i = $i + 3) { ?>
        <div class="js-scroll-content" style="<?php echo $i < 3 ? '' : 'display: none;' ?>">
            <ul class="b-history-list list-unstyled">
                <?php if (isset($history_items[$i])) {
                    $font_icon = $history_items[$i]['icon_' . $history_items[$i]['type']];
                    ?>
                    <li class="b-history-list__item clearfix">
                        <div class="b-history-list__label bg-primary center-block"><?php echo $history_items[$i]['date']; ?></div>
                        <div class="b-history-list__inner">
                            <h3 class="b-history-list__title"><?php echo $history_items[$i]['title']; ?></h3>
                            <div class="b-history-list__description"><?php echo $history_items[$i]['text']; ?></div>
                            <i class="b-history-list__icon color-primary <?php echo $font_icon; ?>"></i>
                        </div>
                    </li>
                <?php } ?>
                <?php if (isset($history_items[$i + 1])) {
                    $font_icon = $history_items[$i + 1]['icon_' . $history_items[$i + 1]['type']];
                    ?>
                    <li class="b-history-list__item clearfix">
                        <div class="b-history-list__label bg-primary center-block"><?php echo $history_items[$i + 1]['date']; ?></div>
                        <div class="b-history-list__inner">
                            <h3 class="b-history-list__title"><?php echo $history_items[$i + 1]['title']; ?></h3>
                            <div class="b-history-list__description"><?php echo $history_items[$i + 1]['text']; ?></div>
                            <i class="b-history-list__icon color-primary <?php echo $font_icon; ?>"></i>
                        </div>
                    </li>
                <?php } ?>
                <?php if (isset($history_items[$i + 2])) {
                    $font_icon = $history_items[$i + 2]['icon_' . $history_items[$i + 2]['type']];
                    ?>
                    <li class="b-history-list__item clearfix">
                        <div class="b-history-list__label bg-primary center-block"><?php echo $history_items[$i + 2]['date']; ?></div>
                        <div class="b-history-list__inner">
                            <h3 class="b-history-list__title"><?php echo $history_items[$i + 2]['title']; ?></h3>
                            <div class="b-history-list__description"><?php echo $history_items[$i + 2]['text']; ?></div>
                            <i class="b-history-list__icon color-primary <?php echo $font_icon; ?>"></i>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>
    <span class="b-history-list__btn btn btn-default btn-sm" id="show_more_history_<?php echo $widget_id;?>"><?php echo $button_text; ?></span>
</div>
