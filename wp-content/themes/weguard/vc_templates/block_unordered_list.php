<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $style
 * @var $bold
 * @var $items [N]['text']
 * Shortcode class
 * @var $this WPBakeryShortCode_Block_Unordered_List
 */

$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$atts = $this->convertAttributesToArray($atts);
extract($atts);
$items = (array)vc_param_group_parse_atts($items); ?>
<<?php echo $style == 'list-num list-num-1'? 'ol' : 'ul'; ?> class="list <?php echo $style; ?> <?php echo $bold; ?>">
    <?php foreach ($items as $data) { ?>
        <li><?php echo $data['text']; ?></li>
    <?php } ?>
</<?php echo $style == 'list-num list-num-1'? 'ol' : 'ul'; ?>>