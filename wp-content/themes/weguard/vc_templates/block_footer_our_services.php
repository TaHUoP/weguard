<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $hidden (unused)
 * Shortcode class
 * @var $this WPBakeryShortCode_Block_Footer_Our_Services
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);

$theme_locations = get_nav_menu_locations();

$menu_items = wp_get_nav_menu_items($theme_locations['our-services-menu']);?>

<section class="footer-section">
    <h3 class="footer-section__title">our services</h3>
    <ul class="footer-section__list list list-mark-4">
        <?php foreach ($menu_items as $menu_item){ ?>
            <li><a href="<?php echo $menu_item->url ?>"><?php echo $menu_item->title ?></a></li>
        <?php } ?>
    </ul>
</section>