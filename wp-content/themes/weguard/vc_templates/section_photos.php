<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $button_text
 * @var $images
 * Shortcode class
 * @var $this WPBakeryShortCode_Section_Photos
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);

$images = explode(',', $images);

$widget_id = mt_rand(1000,9999);
?>

<script>

    jQuery(document).ready(function($) {

        $( "#show_more_images_<?php echo $widget_id;?>" ).on( "click", function() {

            var hiddenContent =  $("#section_photos_<?php echo $widget_id;?>").find('.js-zoom-gallery.js-scroll-content:hidden');
            
            hiddenContent.show();
            
            if($("#section_photos_<?php echo $widget_id;?>").find('.js-zoom-gallery.js-scroll-content:hidden').length == 0){
                $("#show_more_images_<?php echo $widget_id;?>").hide();
            }
            
            hiddenContent.addClass("animated");
            hiddenContent.addClass("animation-done");
            hiddenContent.addClass("bounceInUp");

        });
    });

</script>

<div class="section-default section_border-bottom wow">
    <div class="b-gallery-1" id="section_photos_<?php echo $widget_id;?>">
        <?php for ($i = 0; $i < count($images); $i = $i + 4) { ?>
            <div class="js-zoom-gallery js-scroll-content" style="<?php echo $i <4 ? '' : 'display: none;' ?>">
                <?php if (isset($images[$i])) {
                    $img_id = preg_replace('/[^\d]/', '', $images[$i]);
                    $img_meta_array = weguard_wp_get_attachment($img_id); ?>
                    <div class="b-gallery-1__item">
                        <a href="<?php echo esc_url($img_meta_array['src']); ?>" class="js-zoom-gallery__item">
                            <img src="<?php echo esc_url($img_meta_array['src']); ?>" alt="<?php echo $img_meta_array['alt']; ?>" class="img-responsive"/>
                        </a>
                    </div>
                <?php } ?>
                <?php if (isset($images[$i+1])) {
                    $img_id = preg_replace('/[^\d]/', '', $images[$i+1]);
                    $img_meta_array = weguard_wp_get_attachment($img_id); ?>
                    <div class="b-gallery-1__item">
                        <a href="<?php echo esc_url($img_meta_array['src']); ?>" class="js-zoom-gallery__item">
                            <img src="<?php echo esc_url($img_meta_array['src']); ?>" alt="<?php echo $img_meta_array['alt']; ?>" class="img-responsive"/>
                        </a>
                    </div>
                <?php } ?>
                <?php if (isset($images[$i+2])) {
                    $img_id = preg_replace('/[^\d]/', '', $images[$i+2]);
                    $img_meta_array = weguard_wp_get_attachment($img_id); ?>
                    <div class="b-gallery-1__item">
                        <a href="<?php echo esc_url($img_meta_array['src']); ?>" class="js-zoom-gallery__item">
                            <img src="<?php echo esc_url($img_meta_array['src']); ?>" alt="<?php echo $img_meta_array['alt']; ?>" class="img-responsive"/>
                        </a>
                    </div>
                <?php } ?>
                <?php if (isset($images[$i+3])) {
                    $img_id = preg_replace('/[^\d]/', '', $images[$i+3]);
                    $img_meta_array = weguard_wp_get_attachment($img_id); ?>
                    <div class="b-gallery-1__item">
                        <a href="<?php echo esc_url($img_meta_array['src']); ?>" class="js-zoom-gallery__item">
                            <img src="<?php echo esc_url($img_meta_array['src']); ?>" alt="<?php echo $img_meta_array['alt']; ?>" class="img-responsive"/>
                        </a>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
        <span class="b-gallery-1__btn btn btn-default btn-sm btn-effect" id="show_more_images_<?php echo $widget_id;?>"><?php echo $button_text; ?></span>
    </div>
</div>