<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $title
 * Shortcode class
 * @var $this WPBakeryShortCode_Block_Video
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);
?>
    <div class="b-type-b__media">
        <div class="b-type-b__media-title"><?php echo $title; ?></div>
        <?php do_shortcode($content); ?>
    </div>

    


