<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package weguard
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge"/>
    <meta content="" name="description"/>
    <meta content="" name="keywords"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="telephone=no" name="format-detection"/>
    <meta name="HandheldFriendly" content="true"/>

    <?php wp_head(); ?>
    <link rel="icon" type="image/x-icon" href="favicon.ico"/>
    <script>(function (H) {
            H.className = H.className.replace(/\bno-js\b/, 'js')
        })(document.documentElement)</script>
</head>
<body>
<!-- Loader-->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end-->

<div data-header="sticky" data-header-top="200" class="l-theme animated-css">
    <!-- Start Switcher-->
    <div class="switcher-wrapper">
        <div class="demo_changer">
            <div class="demo-icon color-primary"><i class="fa fa-cog fa-spin fa-2x"></i></div>
            <div class="form_holder">
                <div class="predefined_styles">
                    <div class="skin-theme-switcher">
                        <h4>Color</h4><a href="javascript:void(0);" data-switchcolor="color1" style="background-color:#5aaff7;" class="styleswitch"></a><a href="javascript:void(0);" data-switchcolor="color2" style="background-color:#FFAC3A;" class="styleswitch"></a><a href="javascript:void(0);" data-switchcolor="color3" style="background-color:#28af0f;" class="styleswitch"></a><a href="javascript:void(0);" data-switchcolor="color4" style="background-color:#e425e9;" class="styleswitch"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end switcher-->

    <header class="header <?php echo is_front_page() ? 'header_mod-a' : ''; ?>">
        <div class="header-top clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="social-net list-inline">
                            <li class="social-net__item"><a href="twitter.com" class="social-net__link"><i class="icon fa fa-twitter"></i></a></li>
                            <li class="social-net__item"><a href="facebook.com" class="social-net__link"><i class="icon fa fa-facebook"></i></a></li>
                            <li class="social-net__item"><a href="linkedin.com" class="social-net__link"><i class="icon fa fa-linkedin"></i></a></li>
                            <li class="social-net__item"><a href="vimeo.com" class="social-net__link"><i class="icon fa fa-vimeo"></i></a></li>
                            <li class="social-net__item"><a href="rss.com" class="social-net__link"><i class="icon fa fa-rss"></i></a></li>
                            <li class="social-net__item"><a href="plus.google.com" class="social-net__link"><i class="icon fa fa-google-plus"></i></a></li>
                        </ul>
                        <!-- end social-list-->
                        <div class="header-contact">
                            <div class="header-contact__item">Call :<span class="header-contact__info"> 234 567 8900</span></div>
                            <div class="header-contact__item">Email :<a href="mailto:info@domain.com" class="header-contact__info"> info@domain.com</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-main">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12"><a href="<?php echo get_home_url(); ?>" class="logo"><img src="<?php echo get_theme_mod('header_logo'); ?>" alt="Logo" class="logo__img img-responsive"/></a>
                        <div class="header-nav">
                            <div class="navbar-trigger visible-xs">
                                <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                            </div>
                            <a href="home.html" class="btn-primary header-main__btn btn btn-sm btn-effect">ask a quote</a>
                            <div class="search-trigger"><i class="icon pe-7s-search"></i></div>
                            <nav class="navbar yamm">
                                <div id="navbar-collapse-1" class="navbar-collapse collapse">
                                    <ul class="nav navbar-nav">
                                        <?php
                                        $theme_locations = get_nav_menu_locations();

                                        $menu_items = wp_get_nav_menu_items($theme_locations['primary']);

                                        for ($i = 0; $i < count($menu_items); $i++) {
                                            if ((boolean)$menu_items[$i]->menu_item_parent != (boolean)$menu_items[$i + 1]->menu_item_parent) {
                                                if ((boolean)$menu_items[$i]->menu_item_parent == false) {
                                                    ?>
                                                    <li class="dropdown <?php echo (int)$menu_items[$i]->object_id === get_the_ID() ? 'active' : ''; ?>">
                                                    <a href="<?php echo $menu_items[$i]->url ?>"><?php echo $menu_items[$i]->title ?></a>
                                                    <ul role="menu" class="dropdown-menu">
                                                <?php } else { ?>
                                                    <li class="<?php echo (int)$menu_items[$i]->object_id === get_the_ID() ? 'active' : ''; ?>"><a href="<?php echo $menu_items[$i]->url ?>"><?php echo $menu_items[$i]->title ?></a></li>
                                                    </ul>
                                                    </li>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <li class="<?php echo (int)$menu_items[$i]->object_id === get_the_ID() ? 'active' : ''; ?>"><a href="<?php echo $menu_items[$i]->url ?>"><?php echo $menu_items[$i]->title ?></a></li>
                                            <?php } ?>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <div class="modal-search">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                                                <?php echo get_search_form(); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="modal-search__close close"><i class="fa fa-times"></i></button>
                                </div>
                            </nav>
                            <!-- end nav-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- end header-->

    <?php if (!is_front_page()){
    $meta = get_post_meta(get_the_ID());
    if (!empty($meta)) {
        foreach ($meta as $key => $val) {
            $meta[$key] = maybe_unserialize($val[0]);
        }
    }
    ?>
    <div class="section-title-page parallax-bg parallax-light">
        <ul class="bg-slideshow">
            <li>
                <div style="background-image:url(<?php echo !is_front_page() && is_home() ? get_theme_mod('header_blog_background') : $meta['header_image']?>)" class="bg-slide"></div>
            </li>
        </ul>
        <div class="parallax__inner">
            <div class="container">
                <div class="row">
                    <div class="col-sm-7">
                        <h1 class="b-title-page"><?php echo !is_front_page() && is_home() ? get_theme_mod('header_blog_title') : $meta['header_title']?></h1>
                        <div class="b-title-page__info"><?php echo !is_front_page() && is_home() ? get_theme_mod('header_blog_text') : $meta['header_description']?></div>
                        <!-- end b-title-page-->
                    </div>
                    <div class="col-sm-5">
                        <ol class="breadcrumb">
                            <?php if (function_exists('bcn_display')) {
                                bcn_display();
                            } ?>
<!--                            <li><a href="home.html">home</a></li>-->
<!--                            <li class="active">typography</li>-->
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>