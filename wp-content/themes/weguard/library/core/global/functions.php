<?php 
	function weguard_load_modules($modules = array()){
		if (!is_array($modules))
			return false;
			

		foreach($modules as $_module){
            $_moduleDir = get_template_directory() . '/library/modules/' . $_module . '/';
			if (file_exists($_moduleDir) && is_dir($_moduleDir) && file_exists($_moduleDir . $_module . '.php')){
                get_template_part( 'library/modules/' . $_module . '/' . $_module );
            }
		}

	}

	function weguard_load_files($files,$dir = false){
		if (!is_array($files))
			return false;

		if (!$dir)
			$dir = '';

		foreach($files as $_file){
			$filename = $dir . $_file;
			if (file_exists(get_template_directory() . '/'.$filename.'.php')){
				get_template_part( $filename );
			}
		}
	}



	
?>