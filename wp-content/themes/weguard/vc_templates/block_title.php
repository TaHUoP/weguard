<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $title
 * @var $description
 * @var $color
 * @var $underline
 * @var $style
 * Shortcode class
 * @var $this WPBakeryShortCode_Block_Title
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts); ?>

<div class="text-center">
    <h2 class="ui-title-block" style="color:<?php echo $color; ?>;"><?php echo $title; ?></h2>
    <div class="ui-subtitle-block" style="color:<?php echo $color; ?>;"><?php echo $description; ?></div>
    <?php
    if(!empty($underline) && $style == 'A') {
        echo '<div class="ui-decor-type-1 center-block"></div>';
    }elseif(!empty($underline) && $style == 'B'){
        echo '<div class="ui-decor-wrap"><div class="ui-decor-type-2"></div></div>';
    } ?>
</div>

