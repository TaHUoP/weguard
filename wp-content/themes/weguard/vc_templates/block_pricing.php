<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $style
 * @var $price_label
 * @var $price_number
 * @var $price_title
 * @var $button_link
 * @var $list_items [N]['text']
 * Shortcode class
 * @var $this WPBakeryShortCode_Block_Pricing
 */

$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$atts = $this->convertAttributesToArray($atts);
extract($atts);
$list_items = (array)vc_param_group_parse_atts($list_items);

$button_link = vc_build_link($button_link);
if ($style == 'A') { ?>
    <section class="b-pricing b-pricing_sm">
        <div class="b-pricing__inner">
            <div class="b-pricing-price">
                <div class="b-pricing-price__label"><?php echo $price_label; ?></div>
                <div class="b-pricing-price__number"><?php echo $price_number; ?></div>
            </div>
            <h3 class="b-pricing__title"><?php echo $price_title; ?></h3>
        </div>
        <ul class="b-pricing-list list-unstyled">
            <?php foreach ($list_items as $data) { ?>
                <li class="b-pricing-list__item"><?php echo $data['text']; ?></li>
            <?php } ?>
        </ul>
        <a href="<?php echo $button_link['url']; ?>" class="b-pricing__btn btn btn-primary btn-effect"><?php echo $button_link['title']; ?></a>
    </section>
<?php } elseif ($style == 'B') { ?>
    <section class="b-pricing b-pricing_lg">
        <div class="b-pricing__inner">
            <div class="b-pricing-price">
                <div class="b-pricing-price__label"><?php echo $price_label; ?></div>
                <div class="b-pricing-price__number"><?php echo $price_number; ?></div>
            </div>
            <h3 class="b-pricing__title"><?php echo $price_title; ?></h3>
        </div>
        <ul class="b-pricing-list list-unstyled">
            <?php foreach ($list_items as $data) { ?>
                <li class="b-pricing-list__item"><?php echo $data['text']; ?></li>
            <?php } ?>
        </ul>
        <a href="<?php echo $button_link['url']; ?>" class="b-pricing__btn btn btn-primary btn-effect"><?php echo $button_link['title']; ?></a>
    </section>
<?php } ?>

