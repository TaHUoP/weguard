<?php

add_action('init', 'weguard_integrateWithVC', 200);

function weguard_integrateWithVC()
{

    if (!function_exists('vc_map'))
        return FALSE;


    $attributes = array(
        array(
            'type' => 'dropdown',
            'heading' => "Text Color",
            'param_name' => 'ptextcolor',
            'value' => array("Default", "White", "Black"),
            'description' => esc_html__("Text Color", 'weguard'),
            'group' => esc_html__('Row Options', 'weguard'),
        ),

    );
    vc_add_params('vc_row', $attributes);

    /** Fonts Icon Loader */
    $vc_icons_data = weguard_init_vc_icons();

    /** Class for repeated fields */
    class WPBakeryShortCode_Repeater_Params extends WPBakeryShortCode
    {
        public static function convertAttributesToArray($atts)
        {
            if (isset($atts['values']) && strlen($atts['values']) > 0) {
                $values = vc_param_group_parse_atts($atts['values']);
                if (!is_array($values)) {
                    $temp = explode(',', $atts['values']);
                    $paramValues = array();
                    foreach ($temp as $value) {
                        $data = explode('|', $value);
                        $colorIndex = 2;
                        $newLine = array();
                        $newLine['value'] = isset($data[0]) ? $data[0] : 0;
                        $newLine['label'] = isset($data[1]) ? $data[1] : '';
                        if (isset($data[1]) && preg_match('/^\d{1,3}\%$/', $data[1])) {
                            $colorIndex += 1;
                            $newLine['value'] = (float)str_replace('%', '', $data[1]);
                            $newLine['label'] = isset($data[2]) ? $data[2] : '';
                        }
                        if (isset($data[$colorIndex])) {
                            $newLine['customcolor'] = $data[$colorIndex];
                        }
                        $paramValues[] = $newLine;
                    }
                    $atts['values'] = urlencode(json_encode($paramValues));
                }
            }

            return $atts;
        }
    }

    vc_map(
        array(
            'name' => esc_html__('Block title', 'weguard'),
            'base' => 'block_title',
            'category' => esc_html__('Weguard', 'weguard'),
            'params' => array(
                array(
                    'type' => 'textarea',
                    'heading' => __('Title', 'weguard'),
                    'param_name' => 'title',
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('Description', 'weguard'),
                    'param_name' => 'description',
                ),
                array(
                    "type" => "colorpicker",
                    "heading" => __("Text color", "weguard"),
                    "param_name" => "color",
                    "value" => '#FFFFFF',
                ),
                array(
                    'type' => 'checkbox',
                    'heading' => __('Underline', 'weguard'),
                    'param_name' => 'underline',
                    'description' => __('Show decoration line under the title', 'weguard'),
                    'value' => 'true',
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __('Style', 'weguard'),
                    'param_name' => 'style',
                    'value' => array('A', 'B'),
                    'dependency' => array(
                        'element' => 'underline',
                        'value' => 'true',
                    ),
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Block_Title extends WPBakeryShortCode
        {

        }
    }

    $award_params = array(
        array(
            'type' => 'textfield',
            'heading' => __('Blue title', 'weguard'),
            'param_name' => 'blue_title',
            'dependency' => array(
                'element' => 'style',
                'value' => 'A',
            ),
        ),
        array(
            'type' => 'textfield',
            'heading' => __('Main title', 'weguard'),
            'param_name' => 'main_title',
        ),
        array(
            'type' => 'textarea',
            'heading' => __('Text', 'weguard'),
            'param_name' => 'text',
        ),
        array(
            'type' => 'dropdown',
            'heading' => __('Style', 'weguard'),
            'param_name' => 'style',
            'value' => array('A', 'B'),
        ),
    );
    $params = array_merge($award_params, weguard_get_vc_icons($vc_icons_data));
    vc_map(
        array(
            'name' => esc_html__('Block award', 'weguard'),
            'base' => 'block_award',
            'category' => esc_html__('Weguard', 'weguard'),
            'params' => $params,
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Block_Award extends WPBakeryShortCode
        {

        }
    }

    vc_map(
        array(
            'name' => esc_html__('Block contact us', 'weguard'),
            'base' => 'block_contact_us',
            'category' => esc_html__('Weguard', 'weguard'),
            'params' => array(
                array(
                    'type' => 'textfield',
                    'heading' => __('Title', 'weguard'),
                    'param_name' => 'title',
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('Phone', 'weguard'),
                    'param_name' => 'phone',
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('Email', 'weguard'),
                    'param_name' => 'email',
                ),
//                array(
//                    'type' => 'dropdown',
//                    'heading' => __('Style', 'weguard'),
//                    'param_name' => 'style',
//                    'value' => array('A', 'B'),
//                ),
//                array(
//                    'type' => 'attach_image',
//                    'heading' => esc_html__('Image', 'weguard'),
//                    'param_name' => 'image',
//                    'value' => '',
//                    'description' => esc_html__('Select image from media library.', 'weguard')
//                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Block_Contact_Us extends WPBakeryShortCode
        {

        }
    }

    $advantage_params = array(
        array(
            'type' => 'dropdown',
            'heading' => __('Style', 'weguard'),
            'param_name' => 'style',
            'value' => array('A', 'B'),
        ),
        array(
            'type' => 'checkbox',
            'heading' => esc_html__('Middle line', 'weguard'),
            'param_name' => 'middle_line',
            'description' => esc_html__('Show decoration line to the right of the widget', 'weguard'),
            'value' => 'true',
            'dependency' => array(
                'element' => 'style',
                'value' => 'A',
            ),
        ),
        array(
            'type' => 'textfield',
            'heading' => __('Title', 'weguard'),
            'param_name' => 'title',
        ),
        array(
            'type' => 'textfield',
            'heading' => __('Description', 'weguard'),
            'param_name' => 'description',
            'dependency' => array(
                'element' => 'style',
                'value' => 'B',
            ),
        ),
        array(
            'type' => 'textarea',
            'heading' => __('Text', 'weguard'),
            'param_name' => 'text',
        ),
    );
    $params = array_merge($advantage_params, weguard_get_vc_icons($vc_icons_data));
    vc_map(
        array(
            'name' => esc_html__('Block advantage', 'weguard'),
            'base' => 'block_advantage',
            'category' => esc_html__('Weguard', 'weguard'),
            'params' => $params,
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Block_Advantage extends WPBakeryShortCode
        {

        }
    }

    vc_map(
        array(
            'name' => esc_html__('Section progress list', 'weguard'),
            'base' => 'section_progress_list',
            'category' => esc_html__('Weguard', 'weguard'),
            'params' => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __('Style', 'weguard'),
                    'param_name' => 'style',
                    'value' => array('A', 'B'),
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('First title', 'weguard'),
                    'param_name' => 'first_title',
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('First number', 'weguard'),
                    'param_name' => 'first_number',
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('First description', 'weguard'),
                    'param_name' => 'first_description',
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('Second title', 'weguard'),
                    'param_name' => 'second_title',
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('Second number', 'weguard'),
                    'param_name' => 'second_number',
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('Second description', 'weguard'),
                    'param_name' => 'second_description',
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('Third title', 'weguard'),
                    'param_name' => 'third_title',
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('Third number', 'weguard'),
                    'param_name' => 'third_number',
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('Third description', 'weguard'),
                    'param_name' => 'third_description',
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Section_Progress_List extends WPBakeryShortCode
        {

        }
    }

    vc_map(
        array(
            'name' => esc_html__('Section proposition', 'weguard'),
            'base' => 'section_proposition',
            'category' => esc_html__('Weguard', 'weguard'),
            'params' => array(
                array(
                    "type" => "textfield",
                    "heading" => __("Title", "weguard"),
                    "param_name" => "title",
                ),
                array(
                    "type" => "textfield",
                    "heading" => __("Description", "weguard"),
                    "param_name" => "description",
                ),
                array(
                    "type" => "textarea",
                    "heading" => __("Text", "weguard"),
                    "param_name" => "text",
                ),
                array(
                    "type" => "vc_link",
                    "heading" => __("Button link", "weguard"),
                    "param_name" => "button_link",
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Section_Proposition extends WPBakeryShortCode
        {

        }
    }

    vc_map(
        array(
            'name' => esc_html__('Block progress bar', 'weguard'),
            'base' => 'block_progress_bar',
            'category' => esc_html__('Weguard', 'weguard'),
            'params' => array(
                array(
                    'type' => 'param_group',
                    'heading' => __('Values', 'weguard'),
                    'param_name' => 'values',
                    'description' => __('Enter values for progress bar - value and title', 'weguard'),
                    'params' => array(
                        array(
                            'type' => 'textfield',
                            'heading' => __('Label', 'weguard'),
                            'param_name' => 'label',
                            'description' => __('Enter text used as title of bar.', 'weguard'),
                            'admin_label' => true,
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Value', 'weguard'),
                            'param_name' => 'value',
                            'description' => __('Enter value of bar.', 'weguard'),
                            'admin_label' => true,
                        ),
                    ),
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode_Repeater_Params')) {
        class WPBakeryShortCode_Block_Progress_Bar extends WPBakeryShortCode_Repeater_Params
        {

        }
    }

    vc_map(
        array(
            'name' => esc_html__('Section customers slider', 'weguard'),
            'base' => 'section_customers_slider',
            'category' => esc_html__('Weguard', 'weguard'),
            'params' => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __('Style', 'weguard'),
                    'param_name' => 'style',
                    'value' => array('A', 'B'),
                ),
                array(
                    'type' => 'param_group',
                    'heading' => __('Slides', 'weguard'),
                    'param_name' => 'slides',
                    'params' => array(
                        array(
                            'type' => 'textfield',
                            'heading' => __('Title', 'weguard'),
                            'param_name' => 'title',
                            'admin_label' => true,
                        ),
                        array(
                            'type' => 'textarea',
                            'heading' => __('Text', 'weguard'),
                            'param_name' => 'text',
                            'admin_label' => true,
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Name', 'weguard'),
                            'param_name' => 'name',
                            'admin_label' => true,
                        ),
                        array(
                            'type' => 'textfield',
                            'heading' => __('Company', 'weguard'),
                            'param_name' => 'company',
                            'admin_label' => true,
                        ),
                        array(
                            'type' => 'attach_image',
                            'heading' => esc_html__('Image', 'weguard'),
                            'param_name' => 'image',
                            'value' => '',
                            'description' => esc_html__('Select image from media library.', 'weguard')
                        ),
                    ),
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode_Repeater_Params')) {
        class WPBakeryShortCode_Section_Customers_Slider extends WPBakeryShortCode_Repeater_Params
        {

        }
    }

    vc_map(
        array(
            'name' => esc_html__('Section brand photos', 'weguard'),
            'base' => 'section_brand_photos',
            'category' => esc_html__('Weguard', 'weguard'),
            'params' => array(
                array(
                    'type' => 'attach_images',
                    'heading' => esc_html__('Images', 'weguard'),
                    'param_name' => 'images',
                    'value' => '',
                    'description' => esc_html__('Select images from media library.', 'weguard')
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Section_Brand_Photos extends WPBakeryShortCode
        {

        }
    }

    vc_map(
        array(
            'name' => esc_html__('Block accordion', 'weguard'),
            'base' => 'block_accordion',
            'category' => esc_html__('Weguard', 'weguard'),
            'params' => array(
                array(
                    'type' => 'param_group',
                    'heading' => __('Panels', 'weguard'),
                    'param_name' => 'panels',
                    'description' => __('Enter title and text for accordion panel', 'weguard'),
                    'params' => array(
                        array(
                            "type" => "textfield",
                            "heading" => __("Title", "weguard"),
                            "param_name" => "title",
                        ),
                        array(
                            "type" => "textarea",
                            "heading" => __("Text", "weguard"),
                            "param_name" => "text",
                        ),
                        array(
                            'type' => 'checkbox',
                            'heading' => __('Default', 'weguard'),
                            'param_name' => 'default',
                            'description' => __('Whether panel opened on load or not', 'weguard'),
                            'value' => '',
                        ),
                    ),
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode_Repeater_Params')) {
        class WPBakeryShortCode_Block_Accordion extends WPBakeryShortCode_Repeater_Params
        {

        }
    }

    vc_map(
        array(
            'name' => esc_html__('Section photos', 'weguard'),
            'base' => 'section_photos',
            'category' => esc_html__('Weguard', 'weguard'),
            'params' => array(
                array(
                    "type" => "textfield",
                    "heading" => __("Button text", "weguard"),
                    "param_name" => "button_text",
                ),
                array(
                    'type' => 'attach_images',
                    'heading' => esc_html__('Images', 'weguard'),
                    'param_name' => 'images',
                    'value' => '',
                    'description' => esc_html__('Select images from media library.', 'weguard')
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Section_Photos extends WPBakeryShortCode
        {

        }
    }

    vc_map(
        array(
            'name' => esc_html__('Section news slider', 'weguard'),
            'base' => 'section_news_slider',
            'category' => esc_html__('Weguard', 'weguard'),
            'params' => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __('Style', 'weguard'),
                    'param_name' => 'style',
                    'value' => array('A', 'B'),
                ),
                array(
                    "type" => "textfield",
                    "heading" => __("Posts number", 'weguard'),
                    "param_name" => "post_num",
                    "description" => "Enter posts number to show on page. Use -1 for all posts. "
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Section_News_Slider extends WPBakeryShortCode
        {

        }
    }

    vc_map(
        array(
            'name' => esc_html__('Block send CV', 'weguard'),
            'base' => 'block_send_cv',
            'category' => esc_html__('Weguard', 'weguard'),
            'params' => array(
                array(
                    'type' => 'textfield',
                    'heading' => __('Title', 'weguard'),
                    'param_name' => 'title',
                ),
                array(
                    'type' => 'textarea',
                    'heading' => __('Text', 'weguard'),
                    'param_name' => 'text',
                ),
                array(
                    'type' => 'vc_link',
                    'heading' => esc_html__('Button link', 'weguard'),
                    'param_name' => 'button_link',
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Block_Send_Cv extends WPBakeryShortCode
        {

        }
    }

    vc_map(
        array(
            'name' => esc_html__('Block download brochure', 'weguard'),
            'base' => 'block_download_brochure',
            'category' => esc_html__('Weguard', 'weguard'),
            'params' => array(
                array(
                    'type' => 'textfield',
                    'heading' => __('Title', 'weguard'),
                    'param_name' => 'title',
                ),
                array(
                    'type' => 'textarea',
                    'heading' => __('Text', 'weguard'),
                    'param_name' => 'text',
                ),
                array(
                    'type' => 'vc_link',
                    'heading' => esc_html__('Button link', 'weguard'),
                    'param_name' => 'button_link',
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Block_Download_Brochure extends WPBakeryShortCode
        {

        }
    }

    vc_map(
        array(
            'name' => esc_html__('Block pricing', 'weguard'),
            'base' => 'block_pricing',
            'category' => esc_html__('Weguard', 'weguard'),
            'params' => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __('Style', 'weguard'),
                    'param_name' => 'style',
                    'value' => array('A', 'B'),
                ),
                array(
                    "type" => "textfield",
                    "heading" => __("Price label", "weguard"),
                    "param_name" => "price_label",
                ),
                array(
                    "type" => "textfield",
                    "heading" => __("Price number", "weguard"),
                    "param_name" => "price_number",
                ),
                array(
                    "type" => "textfield",
                    "heading" => __("Price title", "weguard"),
                    "param_name" => "price_title",
                ),
                array(
                    'type' => 'vc_link',
                    'heading' => __('Button link', 'weguard'),
                    'param_name' => 'button_link',
                ),
                array(
                    'type' => 'param_group',
                    'heading' => __('List items', 'weguard'),
                    'param_name' => 'list_items',
                    'params' => array(
                        array(
                            "type" => "textfield",
                            "heading" => __("Text", "weguard"),
                            "param_name" => "text",
                        ),
                    ),
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode_Repeater_Params')) {
        class WPBakeryShortCode_Block_Pricing extends WPBakeryShortCode_Repeater_Params
        {

        }
    }

    $contact_params = array(
        array(
            'type' => 'textfield',
            'heading' => __('Title', 'weguard'),
            'param_name' => 'title',
        ),
        array(
            'type' => 'textarea',
            'heading' => __('Text', 'weguard'),
            'param_name' => 'text',
        ),
    );
    $params = array_merge($contact_params, weguard_get_vc_icons($vc_icons_data));
    vc_map(
        array(
            'name' => esc_html__('Block contact', 'weguard'),
            'base' => 'block_contact',
            'category' => esc_html__('Weguard', 'weguard'),
            'params' => $params,
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Block_Contact extends WPBakeryShortCode
        {

        }
    }

    $history_items_params = array(
        array(
            "type" => "textfield",
            "heading" => __("Date", "weguard"),
            "param_name" => "date",
        ),
        array(
            "type" => "textfield",
            "heading" => __("Title", "weguard"),
            "param_name" => "title",
        ),
        array(
            "type" => "textarea",
            "heading" => __("Text", "weguard"),
            "param_name" => "text",
        ),
    );
    $params = array_merge($history_items_params, weguard_get_vc_icons($vc_icons_data));
    vc_map(
        array(
            'name' => esc_html__('Section history', 'weguard'),
            'base' => 'section_history',
            'category' => esc_html__('Weguard', 'weguard'),
            'params' => array(
                array(
                    'type' => 'textfield',
                    'heading' => __('Button text', 'weguard'),
                    'param_name' => 'button_text',
                ),
                array(
                    'type' => 'param_group',
                    'heading' => __('History items', 'weguard'),
                    'param_name' => 'history_items',
                    'params' => $params,
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode_Repeater_Params')) {
        class WPBakeryShortCode_Section_History extends WPBakeryShortCode_Repeater_Params
        {

        }
    }

    vc_map(
        array(
            'name' => esc_html__('Section employee slider', 'weguard'),
            'base' => 'section_employee_slider',
            'category' => esc_html__('Weguard', 'weguard'),
            'params' => array(
                array(
                    'type' => 'param_group',
                    'heading' => __('Slides', 'weguard'),
                    'param_name' => 'slides',
                    'params' => array(
                        array(
                            "type" => "textfield",
                            "heading" => __("Name", 'weguard'),
                            "param_name" => "name",
                        ),
                        array(
                            "type" => "textfield",
                            "heading" => __("Position", 'weguard'),
                            "param_name" => "position",
                        ),
                        array(
                            "type" => "textarea",
                            "heading" => __("Text", "weguard"),
                            "param_name" => "text",
                        ),
                        array(
                            'type' => 'attach_image',
                            'heading' => esc_html__('Image', 'weguard'),
                            'param_name' => 'image',
                            'value' => '',
                            'description' => esc_html__('Select image from media library.', 'weguard')
                        ),
                        array(
                            "type" => "vc_link",
                            "heading" => __("Twitter link", "weguard"),
                            "param_name" => "twitter_link",
                        ),
                        array(
                            "type" => "vc_link",
                            "heading" => __("Facebook link", "weguard"),
                            "param_name" => "facebook_link",
                        ),
                        array(
                            "type" => "vc_link",
                            "heading" => __("Google plus link", "weguard"),
                            "param_name" => "google_plus_link",
                        ),
                        array(
                            "type" => "vc_link",
                            "heading" => __("Mail link", "weguard"),
                            "param_name" => "mail_link",
                        ),
                    ),
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode_Repeater_Params')) {
        class WPBakeryShortCode_Section_Employee_Slider extends WPBakeryShortCode_Repeater_Params
        {

        }
    }

    $categories = get_categories(array( 'hide_empty' => 1 ));

    $categories_array = array();

    foreach ($categories as $category){
        $categories_array[ $category->name ] = $category->term_id;
    }

    $categories_params = array(
        array(
            "type" => "dropdown",
            "heading" => __("Category", "weguard"),
            "param_name" => "category_id",
            'value' => $categories_array,
        ),
    );
    $params = array_merge($categories_params, weguard_get_vc_icons($vc_icons_data));
    vc_map(
        array(
            'name' => esc_html__('Section categories', 'weguard'),
            'base' => 'section_categories',
            'category' => esc_html__('Weguard', 'weguard'),
            'params' => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __('Style', 'weguard'),
                    'param_name' => 'style',
                    'value' => array('A', 'B'),
                ),
                array(
                    'type' => 'param_group',
                    'heading' => __('Categories', 'weguard'),
                    'param_name' => 'categories',
                    'params' => $params,
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode_Repeater_Params')) {
        class WPBakeryShortCode_Section_Categories extends WPBakeryShortCode_Repeater_Params
        {

        }
    }

    $info_params = array(
        array(
            "type" => "textfield",
            "heading" => __("Title", "weguard"),
            "param_name" => "title",
        ),
        array(
            "type" => "textarea",
            "heading" => __("Text", "weguard"),
            "param_name" => "text",
        ),
    );
    $params = array_merge($info_params, weguard_get_vc_icons($vc_icons_data));
    vc_map(
        array(
            'name' => esc_html__('Block info', 'weguard'),
            'base' => 'block_info',
            'category' => esc_html__('Weguard', 'weguard'),
            'params' => array(
                array(
                    'type' => 'textfield',
                    'heading' => __('Title', 'weguard'),
                    'param_name' => 'title',
                ),
                array(
                    'type' => 'attach_image',
                    'heading' => esc_html__('Logo', 'weguard'),
                    'param_name' => 'logo',
                    'value' => '',
                    'description' => esc_html__('Select image from media library.', 'weguard')
                ),
                array(
                    'type' => 'param_group',
                    'heading' => __('Info items', 'weguard'),
                    'param_name' => 'info_items',
                    'params' => $params,
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode_Repeater_Params')) {
        class WPBakeryShortCode_Block_Info extends WPBakeryShortCode_Repeater_Params
        {

        }
    }

    vc_map(
        array(
            "name" => __("Block footer our services", "weguard"),
            "base" => "block_footer_our_services",
            'category' => __('Weguard', 'weguard'),
            "params" => array(
                array(
                    "type" => "textfield",
                    "heading" => __("Add menu in \"Appearance\" -> \"Menus\" to our services location", "weguard"),
                    "param_name" => "hidden",
                    "holder" => "hidden input",
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Block_Footer_Our_Services extends WPBakeryShortCode
        {
        }
    }

    vc_map(
        array(
            "name" => __("Block footer areas served", "weguard"),
            "base" => "block_footer_areas_served",
            'category' => __('Weguard', 'weguard'),
            "params" => array(
                array(
                    "type" => "textfield",
                    "heading" => __("Add menu in \"Appearance\" -> \"Menus\" to areas served location", "weguard"),
                    "param_name" => "hidden",
                    "holder" => "hidden input",
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Block_Footer_Areas_Served extends WPBakeryShortCode
        {
        }
    }
    
    
    $social_params = array(
        array(
            "type" => "vc_link",
            "heading" => __("Link", "weguard"),
            "param_name" => "link",
        ),
    );
    $params = array_merge($social_params, weguard_get_vc_icons($vc_icons_data));
    vc_map(
        array(
            'name' => esc_html__('Block footer social icons', 'weguard'),
            'base' => 'block_footer_social_icons',
            'category' => esc_html__('Weguard', 'weguard'),
            'params' => array(
                array(
                    'type' => 'param_group',
                    'heading' => __('Icons', 'weguard'),
                    'param_name' => 'icons',
                    'params' => $params,
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode_Repeater_Params')) {
        class WPBakeryShortCode_Block_Footer_Social_Icons extends WPBakeryShortCode_Repeater_Params
        {

        }
    }

// security features blocks
    $security_features_params = array(
        array(
            'type' => 'textfield',
            'heading' => __('Title', 'weguard'),
            'param_name' => 'title',
        ),
        array(
            'type' => 'textarea',
            'heading' => __('Text', 'weguard'),
            'param_name' => 'text',
        )
    );
    $params = array_merge($security_features_params, weguard_get_vc_icons($vc_icons_data));

    vc_map(
        array(
            'name' => esc_html__('Block categories', 'weguard'),
            'base' => 'block_categories',
            'category' => esc_html__('Weguard', 'weguard'),
            'params' => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __('Style', 'weguard'),
                    'param_name' => 'style',
                    'value' => array('Left', 'Right'),
                ),
                array(
                    'type' => 'param_group',
                    'heading' => __('Categories', 'weguard'),
                    'param_name' => 'categories',
                    'params' => $params,
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode_Repeater_Params')) {
        class WPBakeryShortCode_Block_Categories extends WPBakeryShortCode_Repeater_Params
        {

        }
    }
// end security features blocks

    vc_map(
        array(
            'name' => esc_html__('Block video', 'weguard'),
            'base' => 'block_video',
            "as_parent" => array('only' => 'vc_video'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
            "content_element" => true,
            "show_settings_on_create" => true,
            "is_container" => true,
            'category' => esc_html__('Weguard', 'weguard'),
            'params' => array(
                array(
                    'type' => 'textfield',
                    'heading' => __('Title', 'weguard'),
                    'param_name' => 'title',
                ),
            ),
            'js_view' => 'VcColumnView',
        )
    );

    if (class_exists('WPBakeryShortCodesContainer')) {
        class WPBakeryShortCode_Block_Video extends WPBakeryShortCodesContainer
        {

        }
    }

    vc_map(
        array(
            'name' => __('Block unordered list', 'weguard'),
            'base' => 'block_unordered_list',
            'category' => __('Weguard', 'weguard'),
            'params' => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __('Style', 'weguard'),
                    'param_name' => 'style',
                    'value' => array(
                        '1' => 'list-mark-1',
                        '2' => 'list-mark-2',
                        '3' => 'list-mark-3',
                        '4' => 'list-mark-4',
                        '5' => 'list-mark-5',
                        '6' => 'list-mark-6',
                        '7' => 'list-mark-7',
                        '8' => 'list-mark-8',
                        '9' => 'list-num list-num-1',
                    ),
                ),
                array(
                    'type' => 'checkbox',
                    'heading' => __('Bold', 'weguard'),
                    'param_name' => 'bold',
                    'value' => 'list_dark',
                ),
                array(
                    'type' => 'param_group',
                    'heading' => __('Items', 'weguard'),
                    'param_name' => 'items',
                    'params' => array(
                        array(
                            'type' => 'textarea',
                            'heading' => __('Text', 'weguard'),
                            'param_name' => 'text',
                        ),
                    ),
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode_Repeater_Params')) {
        class WPBakeryShortCode_Block_Unordered_List extends WPBakeryShortCode_Repeater_Params
        {

        }
    }

    vc_map(
        array(
            'name' => __('Block blockquote', 'weguard'),
            'base' => 'block_blockquote',
            'category' => __('Weguard', 'weguard'),
            'params' => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __('Style', 'weguard'),
                    'param_name' => 'style',
                    'value' => array('A', 'B', 'C', 'D'),
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('Title', 'weguard'),
                    'param_name' => 'title',
                    'dependency' => array(
                        'element' => 'style',
                        'value' => 'A',
                    ),
                ),
                array(
                    'type' => 'textarea',
                    'heading' => __('Text', 'weguard'),
                    'param_name' => 'text',
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('Name', 'weguard'),
                    'param_name' => 'name',
                    'dependency' => array(
                        'element' => 'style',
                        'value' => array('A', 'B', 'D'),
                    ),
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('Position', 'weguard'),
                    'param_name' => 'position',
                    'dependency' => array(
                        'element' => 'style',
                        'value' => array('A', 'B', 'D'),
                    ),
                ),
                array(
                    'type' => 'attach_image',
                    'heading' => __('Image', 'weguard'),
                    'param_name' => 'image',
                    'value' => '',
                    'description' => __('Select image from media library.', 'weguard'),
                    'dependency' => array(
                        'element' => 'style',
                        'value' => array('A', 'B', 'D'),
                    ),
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Block_Blockquote extends WPBakeryShortCode
        {

        }
    }

}


?>