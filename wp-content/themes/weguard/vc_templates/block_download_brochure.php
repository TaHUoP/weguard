<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $title
 * @var $text
 * @var $button_link
 * Shortcode class
 * @var $this WPBakeryShortCode_Block_Download_Brochure
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);

$button_link = vc_build_link( $button_link );
?>

<div class="b-container-without-1 b-container-without-1_mod-b border-2-colors bg-secondary">
    <h2 class="b-container-without-1__title"><?php echo $title; ?></h2>
    <div class="b-container-without-1__text">
        <p><?php echo $text; ?></p>
    </div>
    <a href="<?php echo $button_link['url']; ?>" class="b-container-without-1__btn btn btn-default btn-effect"><?php echo $button_link['title']; ?></a>
</div>

