<?php
	/**  Theme_index  **/

	/* Define library Theme path */

    $weguard_themeFiles = array(        
	    'vc_templates',	
	    'functions'   
    );

    weguard_load_files($weguard_themeFiles, 'library/theme/');

	if ( ! isset( $content_width ) ) {
		$content_width = 600;
	}

?>