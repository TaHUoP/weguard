<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $slides [N]['title']
 * @var $slides [N]['name']
 * @var $slides [N]['position']
 * @var $slides [N]['text']
 * @var $slides [N]['image']
 * @var $slides [N]['twitter_link']
 * @var $slides [N]['facebook_link']
 * @var $slides [N]['google_plus_link']
 * @var $slides [N]['mail_link']
 * Shortcode class
 * @var $this WPBakeryShortCode_Section_Employee_Slider
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$atts = $this->convertAttributesToArray($atts);
extract($atts);
$slides = (array)vc_param_group_parse_atts($slides);

$button_link = vc_build_link($button_link);
?>

<div data-min480="1" data-min768="2" data-min992="2" data-min1200="3" data-pagination="true"
     data-navigation="false" data-auto-play="4000" data-stop-on-hover="true"
     class="owl-carousel owl-theme enable-owl-carousel">
    <?php foreach ($slides as $slide) {
        $img_id = preg_replace('/[^\d]/', '', $slide['image']);
        $img_meta_array = weguard_wp_get_attachment($img_id);
        $twitter_link = vc_build_link($slide['twitter_link']);
        $facebook_link = vc_build_link($slide['facebook_link']);
        $google_plus_link = vc_build_link($slide['google_plus_link']);
        $mail_link = vc_build_link($slide['mail_link']);
        ?>
        <section class="b-team">
            <div class="b-team__media">
                <img src="<?php echo $img_meta_array['src']; ?>" alt="<?php echo $img_meta_array['alt']; ?>" class="img-responsive"/>
            </div>
            <h3 class="b-team__name"><?php echo $slide['name']; ?></h3>
            <div class="b-team__category"><?php echo $slide['position']; ?></div>
            <div class="b-team__description"><?php echo $slide['text']; ?></div>
            <ul class="social-net list-inline">
                <?php if (!empty($twitter_link['url'])) { ?>
                    <li class="social-net__item">
                        <a href="<?php echo $twitter_link['url']; ?>" class="social-net__link"><i class="icon fa fa-twitter"></i></a>
                    </li>
                <?php } if (!empty($facebook_link['url'])) { ?>
                    <li class="social-net__item">
                        <a href="<?php echo $facebook_link['url']; ?>" class="social-net__link"><i class="icon fa fa-facebook"></i></a>
                    </li>
                <?php } if (!empty($google_plus_link['url'])) { ?>
                    <li class="social-net__item">
                        <a href="<?php echo $google_plus_link['url']; ?>" class="social-net__link"><i class="icon fa fa-google-plus"></i></a>
                    </li>
                <?php } if (!empty($mail_link['url'])) { ?>
                    <li class="social-net__item">
                        <a href="<?php echo $mail_link['url']; ?>" class="social-net__link"><i class="icon fa fa-envelope"></i></a>
                    </li>
                <?php } ?>
            </ul>
            <!-- end social-list-->
        </section>
    <?php } ?>
</div>
