<?php
/**
 * weguard functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package weguard
 */

if (!function_exists('weguard_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function weguard_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on weguard, use a find and replace
         * to change 'weguard' to the name of your theme in all the template files.
         */
        load_theme_textdomain('weguard', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'primary' => esc_html__('Primary', 'weguard'),
            'areas-served-menu' => esc_html__('Areas served', 'weguard'),
            'our-services-menu' => esc_html__('Our services', 'weguard'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('weguard_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));
    }
endif;
add_action('after_setup_theme', 'weguard_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function weguard_content_width()
{
    $GLOBALS['content_width'] = apply_filters('weguard_content_width', 640);
}

add_action('after_setup_theme', 'weguard_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function weguard_widgets_init()
{
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'weguard'),
        'id' => 'sidebar-1',
        'description' => esc_html__('Add widgets here.', 'weguard'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'weguard_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function weguard_scripts()
{
//	wp_enqueue_style( 'weguard-style', get_stylesheet_uri() );

    wp_enqueue_style('master', get_template_directory_uri() . '/css/master.css');

    wp_enqueue_style('switcher-css', get_template_directory_uri() . '/js/switcher/css/switcher.css');
    wp_enqueue_style('switcher-color1', get_template_directory_uri() . '/js/switcher/css/color1.css');
//	wp_enqueue_style( 'switcher-color2', get_template_directory_uri() . '/js/switcher/css/color2.css');
//	wp_enqueue_style( 'switcher-color3', get_template_directory_uri() . '/js/switcher/css/color3.css');
//	wp_enqueue_style( 'switcher-color4', get_template_directory_uri() . '/js/switcher/css/color4.css');


    wp_enqueue_script('weguard-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true);

    wp_enqueue_script('weguard-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true);

    wp_enqueue_script('jquery-min', get_template_directory_uri() . '/js/jquery-1.11.3.min.js', array(), '20151215', true);

    wp_enqueue_script('jquery-ui-min', get_template_directory_uri() . '/js/jquery-ui-1.12.0.custom/jquery-ui.min.js', array(), '20151215', true);

    wp_enqueue_script('custom-js', get_template_directory_uri() . '/js/separate-js/custom.js', array(), '20151215', true);

    wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap/bootstrap.min.js', array(), '20151215', true);
    wp_enqueue_script('jquery-sliderPro', get_template_directory_uri() . '/js/slider-pro/jquery.sliderPro.min.js', array(), '20151215', true);
    wp_enqueue_script('owl-carousel', get_template_directory_uri() . '/js/owl-carousel/owl.carousel.min.js', array(), '20151215', true);
    wp_enqueue_script('jquery-magnific-popup', get_template_directory_uri() . '/js/magnific-popup/jquery.magnific-popup.min.js', array(), '20151215', true);
    wp_enqueue_script('bootstrap-select', get_template_directory_uri() . '/js/bootstrap-select/dist/js/bootstrap-select.min.js', array(), '20151215', true);
    wp_enqueue_script('doubletaptogo', get_template_directory_uri() . '/js/doubletaptogo.js', array(), '20151215', true);
    wp_enqueue_script('waypoints', get_template_directory_uri() . '/js/waypoints.min.js', array(), '20151215', true);
    wp_enqueue_script('flowplayer', get_template_directory_uri() . '/js/flowplayer/flowplayer.min.js', array(), '20151215', true);
    wp_enqueue_script('classie', get_template_directory_uri() . '/js/classie.js', array(), '20151215', true);
    wp_enqueue_script('scrollreveal', get_template_directory_uri() . '/js/scrollreveal/scrollreveal.min.js', array(), '20151215', true);
    wp_enqueue_script('dmss', get_template_directory_uri() . '/js/switcher/js/dmss.js', array(), '20151215', true);
    wp_enqueue_script('jarallax', get_template_directory_uri() . '/js/jarallax/jarallax.min.js', array(), '20151215', true);
    wp_enqueue_script('jquery.easypiechart', get_template_directory_uri() . '/js/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js', array(), '20151215', true);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'weguard_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/* Include main framework file */
require_once(get_template_directory() . '/library/loader.php');

//function weguard_create_post_type()
//{
//	register_post_type('news',
//		array(
//			'labels' => array(
//				'name' => __('News', 'weguard'),
//				'singular_name' => __('New', 'weguard')
//			),
//			'public' => true,
//			'has_archive' => true,
//			'supports' => array('title', 'editor', 'thumbnail', 'excerpt')
//		)
//	);
//}
//
//add_action('init', 'weguard_create_post_type');

function weguard_register_static_posts_page_metabox()
{
    $cmb = new_cmb2_box(array(
        'id' => 'static_posts_page_metabox',
        'title' => __('Static posts page meta', 'weguard'),
        'object_types' => array('page'), // post type
        'show_on' => array('key' => 'page-template', 'value' => 'home.php'),
        'context' => 'normal', //  'normal', 'advanced', or 'side'
        'priority' => 'high',  //  'high', 'core', 'default' or 'low'
        'show_names' => true, // Show field names on the left
    ));

    $cmb->add_field(array(
        'name' => 'Style',
        'id' => 'blog_style_select',
        'type' => 'select',
        'default' => 'custom',
        'options' => array(
            'one_column' => __('One column', 'weguard'),
            'two_columns' => __('Two columns', 'weguard'),
        ),
    ));
}

add_action('cmb2_admin_init', 'weguard_register_static_posts_page_metabox');

function weguard_register_page_metabox()
{
    $cmb = new_cmb2_box(array(
        'id' => 'page_metabox',
        'title' => __('Page meta', 'weguard'),
        'object_types' => array('page'), // post type
        'context' => 'normal', //  'normal', 'advanced', or 'side'
        'priority' => 'high',  //  'high', 'core', 'default' or 'low'
        'show_names' => true, // Show field names on the left
    ));

    $cmb->add_field(array(
        'name' => __('Header image', 'weguard'),
        'id' => 'header_image',
        'type' => 'file',
    ));

    $cmb->add_field(array(
        'name' => __('Header title', 'weguard'),
        'id' => 'header_title',
        'type' => 'text',
    ));

    $cmb->add_field(array(
        'name' => __('Header description', 'weguard'),
        'id' => 'header_description',
        'type' => 'textarea_small',
    ));
}

add_action('cmb2_admin_init', 'weguard_register_page_metabox');

/*-----WP CUSTOMIZER------*/

function weguard_customizer_register($wp_customize)
{
    $post_list = array();
    $args = array('post_type' => 'staticblocks');
    $posts = get_posts($args);
    foreach ($posts as $post) {
        $post_list[$post->ID] = $post->post_title;
    }

    $wp_customize->remove_section('header_image');
    $wp_customize->remove_section('background_image');
    $wp_customize->remove_control('header_textcolor');

    /*FOOTER SECTION*/

    $wp_customize->add_section('footer_section', array(
        'title' => __('Footer', 'weguard')
    ));

    $wp_customize->add_setting('footer_choice_section', array(
        'sanitize_callback' => 'esc_html',
    ));
    $wp_customize->add_control('footer_choice_section', array(
        'label' => esc_html__('Select footer style', 'weguard'),
        'type' => 'select',
        'section' => 'footer_section',
        'choices' => $post_list,
    ));

    $wp_customize->add_setting('footer_logo', array(
        'default' => '',
        'sanitize_callback' => 'esc_url_raw',
    ));
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'footer_logo', array(
        'label' => __('Footer logo', 'weguard'),
        'section' => 'footer_section',
        'settings' => 'footer_logo',
    )));

    $wp_customize->add_setting('footer_phone', array(
        'default' => '',
        'sanitize_callback' => 'esc_html',
    ));
    $wp_customize->add_control('footer_phone', array(
        'label' => __('Footer phone', 'weguard'),
        'section' => 'footer_section',
        'settings' => 'footer_phone',
    ));

    $wp_customize->add_setting('footer_email', array(
        'default' => '',
        'sanitize_callback' => 'esc_html',
    ));
    $wp_customize->add_control('footer_email', array(
        'label' => __('Footer email', 'weguard'),
        'section' => 'footer_section',
        'settings' => 'footer_email',
    ));

    $wp_customize->add_setting('footer_address', array(
        'default' => '',
        'sanitize_callback' => 'esc_html',
    ));
    $wp_customize->add_control('footer_address', array(
        'label' => __('Footer address', 'weguard'),
        'section' => 'footer_section',
        'settings' => 'footer_address',
    ));

    $wp_customize->add_setting('footer_form', array(
        'default' => '',
        'sanitize_callback' => 'esc_html',
    ));
    $wp_customize->add_control('footer_form', array(
        'label' => __('Footer form shortcode', 'weguard'),
        'section' => 'footer_section',
        'settings' => 'footer_form',
    ));

    /*HEADER SECTION*/

    $wp_customize->add_section('header_section', array(
        'title' => __('Header', 'weguard')
    ));

    $wp_customize->add_setting('header_logo', array(
        'default' => '',
        'sanitize_callback' => 'esc_url_raw',
    ));
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'header_logo', array(
        'label' => __('Header logo', 'weguard'),
        'section' => 'header_section',
        'settings' => 'header_logo',
    )));

    $wp_customize->add_setting('header_blog_background', array(
        'default' => '',
        'sanitize_callback' => 'esc_url_raw',
    ));
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'header_blog_background', array(
        'label' => __('Header blog image', 'weguard'),
        'section' => 'header_section',
        'settings' => 'header_blog_background',
    )));

    $wp_customize->add_setting('header_blog_title', array(
        'default' => '',
        'sanitize_callback' => 'esc_html',
    ));
    $wp_customize->add_control('header_blog_title', array(
        'label' => __('Header blog title', 'weguard'),
        'section' => 'header_section',
        'settings' => 'header_blog_title',
    ));

    $wp_customize->add_setting('header_blog_text', array(
        'default' => '',
        'sanitize_callback' => 'esc_html',
    ));
    $wp_customize->add_control('header_blog_text', array(
        'label' => __('Header blog text', 'weguard'),
        'section' => 'header_section',
        'settings' => 'header_blog_text',
        'type' => 'textarea',
    ));

}

add_action('customize_register', 'weguard_customizer_register');