<?php
/**
 * The template for displaying comments.
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package weguard
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<section class="section-comment">
    <div class="wrap-title">
        <h2 class="ui-title-type-1 ui-title-type-1_sm">post comments (<?php echo get_comments_number(); ?>)</h2>
        <div class="ui-decor-wrap">
            <div class="ui-decor-type-2"></div>
        </div>
    </div>
    <ul class="comments-list list-unstyled">
        <?php $args = array(
                'post_id' => get_the_ID(),
        );
        $comments = get_comments($args);
        foreach ($comments as $comment) { ?>

            <li>
                <article class="comment clearfix">
                    <div class="comment-avatar">
                        <?php echo get_avatar($comment->comment_author_email); ?>
                    </div>
                    <div class="comment-inner">
                        <header class="comment-header">
                            <cite class="comment-author"><?php echo $comment->comment_author; ?></cite>
                            <time datetime="2012-10-27" class="comment-datetime"><?php comment_date('j M Y,', $comment->comment_ID); ?> at <?php comment_date('g:ia', $comment->comment_ID); ?>31 May 2016, at 3:00pm</time>
                        </header>
                        <div class="comment-body">
                            <p><?php echo $comment->comment_content; ?></p>
                        </div>
                    </div>
                </article>
            </li>
        <?php } ?>
    </ul>
</section>