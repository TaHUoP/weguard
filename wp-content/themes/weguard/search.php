<?php
/**
 * The search template file.
 *
 * @package weguard
 */

get_header(); ?>


    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="l-main-content l-main-content_pd-rgt l-main-content_pd-top_lg">

                    <?php
                    if (have_posts()) :?>
                        <div class="posts-group">
                            <!--                        <div class="posts-group_2-col">-->
                            <?php while (have_posts()) : the_post();

                                get_template_part('template-parts/content', 'blog_1');
//                                get_template_part('template-parts/content', 'blog_2');

                            endwhile; ?>
                        </div>
                        <?php
//                        the_posts_navigation();
                        echo get_the_posts_pagination(array(
                                'mid_size' => 1,
                                'prev_text' => __('Newer', 'weguard'),
                                'next_text' => __('Older', 'weguard'),
                        ));

                    endif; ?>

                </div>
            </div>
            <div class="col-md-3">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>

<?php
get_footer();
