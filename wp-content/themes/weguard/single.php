<?php
/**
 *
 * Single post page
 *
 */
?>
<?php get_header(); ?>

<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="l-main-content l-main-content_pd-rgt l-main-content_pd-top_lg">
                <div class="posts-group">
                    <?php
                    while (have_posts()) :
                        the_post(); ?>
                        <article class="b-post b-post-4 clearfix">
                            <div class="entry-media">
                                <a href="<?php the_post_thumbnail_url(); ?>" class="js-zoom-images">
                                    <?php echo get_the_post_thumbnail(get_the_ID()); ?>
                                </a>
                            </div>
                            <div class="entry-main">
                                <div class="entry-header">
                                    <div class="entry-date"><?php echo get_the_date('j M Y'); ?></div>
                                    <div class="entry-meta">
                                    <span class="entry-meta__item">
                                        <i class="entry-meta__icon pe-7s-user"></i>By <?php echo get_the_author_link(); ?>
                                    </span>
                                    <span class="entry-meta__item">
                                        <i class="entry-meta__icon pe-7s-comment"></i>Comments:<a href="<?php echo get_the_permalink() . '#comments'; ?>" class="entry-meta__link"> <?php echo get_comments_number(); ?></a>
                                    </span>
                                    <span class="entry-meta__item">
                                        <i class="entry-meta__icon pe-7s-portfolio"></i>
                                        <?php
                                        $categories = wp_get_post_categories(get_the_ID(), array('fields' => 'all'));
                                        foreach ($categories as $index => $category) {
                                            $delimiter = ($index + 1) == count($categories) ? '' : ', ';
                                            echo '<a href="' . get_category_link($category->term_id) . '" class="entry-meta__link">' . $category->name . $delimiter . '</a>';
                                        }
                                        ?>
                                    </span>
                                    </div>
                                    <h2 class="entry-title entry-title_spacing ui-title-inner"><?php echo get_the_title(); ?></h2>
                                </div>
                                <div class="entry-content">
                                    <?php echo do_shortcode(get_the_content()); ?>
                                </div>
                                <div class="entry-footer">
                                    <div class="post-tags">
                                        <div class="post-tags__title">tags:</div>
                                        <?php
                                        $tags = wp_get_post_tags(get_the_ID(), array('fields' => 'all'));
                                        foreach ($tags as $index => $tag) {
                                            $delimiter = ($index + 1) == count($tags) ? '' : ', ';
                                            echo '<a href="' . get_tag_link($tag->term_id) . '" class="post-tags___item">' . $tag->name . $delimiter . '</a>';
                                        }
                                        ?>
                                    </div>
                                    <a href="#" class="entry-footer__link"><i class="icon pe-7s-share"></i> Share This</a>
                                </div>
                            </div>
                        </article>
                        <!-- end post-->

                        <article class="about-author clearfix">
                            <div class="wrap-title">
                                <h2 class="ui-title-type-1 ui-title-type-1_sm">about the author</h2>
                                <div class="ui-decor-wrap">
                                    <div class="ui-decor-type-2"></div>
                                </div>
                            </div>
                            <div class="about-author__img"><img src="assets/media/content/posts/about-author/1.jpg" alt="foto" class="img-responsive"/></div>
                            <div class="about-author__inner">
                                <div class="about-author__header"><span class="about-author__title">Anam gillson</span><span class="about-author__category">/  security officer</span></div>
                                <div class="about-author__description">Labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi aliquip ex ea commodo consequat. Duis aute irure dolor reprehenderit in voluptate velit esse cillum dolore eu fugiat nu pariatur excepteur sint occacat cupidatat non proident sunt in culpa.</div>
                                <footer class="about-author__footer">
                                    <ul class="social-net list-inline">
                                        <li class="social-net__item"><a href="twitter.com" class="social-net__link"><i class="icon fa fa-twitter"></i></a></li>
                                        <li class="social-net__item"><a href="facebook.com" class="social-net__link"><i class="icon fa fa-facebook"></i></a></li>
                                        <li class="social-net__item"><a href="linkedin.com" class="social-net__link"><i class="icon fa fa-linkedin"></i></a></li>
                                    </ul>
                                    <!-- end social-list-->
                                </footer>
                            </div>
                        </article>
                        <!-- end about-author-->

                        <?php comments_template( '/comments.php' ); ?>
                        <!-- end section-comment-->

                        <?php if (comments_open(get_the_ID())) { ?>
                        <section id="section-reply-form" class="section-reply-form">
                        <div class="wrap-title">
                            <h2 class="ui-title-type-1 ui-title-type-1_sm">leave your reply</h2>
                            <div class="ui-decor-wrap">
                                <div class="ui-decor-type-2"></div>
                            </div>
                        </div>
                        <?php
                        comment_form(
                                array(
                                        'fields' => array(
                                                'author' => '<div class="row">
                                    <div class="col-md-6">
                                        <input type="text" placeholder="Name" class="form-control"/>
                                    </div>',
                                                'email' =>
                                                        '<div class="col-md-6">
                                        <input type="email" placeholder="Email" class="form-control"/>
                                    </div>
                                </div>',
                                                'url' =>
                                                        '',
                                        ),
                                        'comment_field' => '<div class="row">
                                    <div class="col-xs-12">
                                        <textarea rows="6" placeholder="Comments" class="form-control"></textarea>
                                    </div>
                                </div>',
                                        'title_reply' => '',
                                        'label_submit' => 'post comment',
                                        'class_submit' => 'ui-form__btn btn btn-primary btn-sm btn-effect',
                                ),
                                get_the_ID()
                        ); ?>
                    <?php } ?>


                        </section>
                        <!-- end section-reply-form-->

                        <?php
                    endwhile; // End of the loop.
                    ?>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <?php get_sidebar(); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>





