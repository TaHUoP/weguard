<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $panels[N]['title']
 * @var $panels[N]['text']
 * @var $panels[N]['default']
 * Shortcode class
 * @var $this WPBakeryShortCode_Block_Accordion
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$atts = $this->convertAttributesToArray( $atts );
extract($atts);
$panels = (array) vc_param_group_parse_atts( $panels );
?>
<div id="accordion-1" class="panel-group accordion accordion_marg-top_a">
    <?php foreach ($panels as $key => $data){z?>
        <div class="panel <?php echo $data['default'] ? 'panel-default' : ''; ?>">
            <div class="panel-heading">
                <a data-toggle="collapse" data-parent="#accordion-1" href="#collapse-<?php echo $key; ?>" class="btn-collapse <?php echo $data['default'] ? '' : 'collapsed'; ?>">
                    <i class="icon"></i>
                </a>
                <h3 class="panel-title"><?php echo $data['title']; ?></h3>
            </div>
            <div id="collapse-<?php echo $key; ?>" class="panel-collapse collapse <?php echo $data['default'] ? 'in' : ''; ?>">
                <div class="panel-body">
                    <p><?php echo $data['text']; ?></p>
                </div>
            </div>
        </div>
    <?php } ?>
</div>

<!--<div id="accordion-1" class="panel-group accordion accordion_marg-top_a">-->
<!--    --><?php //foreach ($panels as $key => $data){
//        var_dump($data);
//        $font_icon = $data['icon_' . $data['type']];?>
<!--        <div class="panel --><?php //echo $data['default'] ? 'panel-default' : ''; ?><!--">-->
<!--            <div class="panel-heading">-->
<!--                <a data-toggle="collapse" data-parent="#accordion-1" href="#collapse---><?php //echo $key; ?><!--" class="btn-collapse --><?php //echo $data['default'] ? '' : 'collapsed'; ?><!--">-->
<!--                    <i class="--><?php //echo $font_icon; ?><!--"></i>-->
<!--                </a>-->
<!--                <h3 class="panel-title">--><?php //echo $data['title']; ?><!--</h3>-->
<!--            </div>-->
<!--            <div id="collapse---><?php //echo $key; ?><!--" class="panel-collapse collapse --><?php //echo $data['default'] ? 'in' : ''; ?><!--">-->
<!--                <div class="panel-body">-->
<!--                    <p>--><?php //echo $data['text']; ?><!--</p>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    --><?php //} ?>
<!--</div>-->

