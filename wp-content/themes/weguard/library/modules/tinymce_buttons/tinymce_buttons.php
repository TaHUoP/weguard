<?php
add_action( 'init', 'weguard_buttons' );

function weguard_buttons() {
    add_filter( "mce_external_plugins", "weguard_add_buttons" );
    add_filter( 'mce_buttons', 'weguard_register_buttons' );
}

function weguard_add_buttons( $plugin_array ) {
    $plugin_array['weguard'] = get_template_directory_uri() . '/js/weguard-tinymce-buttons.js';
    return $plugin_array;
}

function weguard_register_buttons( $buttons ) {
    array_push( $buttons, 'dropcap', 'highlight' ); // dropcap
    return $buttons;
}