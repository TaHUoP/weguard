<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $middle_line
 * @var $title
 * @var $description
 * @var $text
 * @var $type
 * Shortcode class
 * @var $this WPBakeryShortCode_Block_Advantage
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);

$font_icon = ${'icon_' . $type};
if ($style == 'A') { ?>
    <section class="b-advantages-1-item">

        <?php echo $middle_line ? '<span class="b-advantages-1-item__decor"></span>' : ''; ?>

        <i class="b-advantages-1-item__icon <?php echo $font_icon; ?>"></i>
        <h3 class="b-advantages-1-item__title"><?php echo $title; ?></h3>
        <div class="b-advantages-1-item__text">
            <p><?php echo $text; ?></p>
        </div>
    </section>
<?php } elseif ($style == 'B'){ ?>
    <li class="b-advantages-3__item">
        <div class="b-advantages-3__inner">
            <i class="b-advantages-3__icon color-primary <?php echo $font_icon; ?>"></i>
            <div class="b-advantages-3__subtitle"><?php echo $description; ?></div>
            <div class="b-advantages-3__title"><?php echo $title; ?></div>
        </div>
        <div class="b-advantages-3__text"><?php echo $text; ?></div>
    </li>
<?php } ?>
