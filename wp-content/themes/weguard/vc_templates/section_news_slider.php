<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $style
 * @var $post_num
 * Shortcode class
 * @var $this WPBakeryShortCode_Section_News_Slider
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);

$args = array(
        'post_type' => 'post',
        'orderby' => 'post_date',
        'order' => 'DESC',
        'paged' => 1,
        'posts_per_page' => (int)$post_num,
);
$the_query = new WP_Query($args);
if ($the_query->have_posts()) {
    if ($style == 'A') {
        ?>
        <div data-min480="1" data-min768="2" data-min992="2" data-min1200="3" data-pagination="true"
             data-navigation="false" data-auto-play="4000" data-stop-on-hover="true"
             class="owl-carousel owl-theme enable-owl-carousel">
            <?php while ($the_query->have_posts()) {
                $the_query->the_post(); ?>
                <article class="b-post b-post-1 clearfix">
                    <div class="entry-media">
                        <a href="<?php the_post_thumbnail_url(); ?>" class="js-zoom-images">
                            <img src="<?php the_post_thumbnail_url(); ?>" alt="Foto" class="img-responsive"/>
                        </a>
                    </div>
                    <div class="entry-main">
                        <div class="entry-header">
                            <div class="entry-date"><?php echo get_the_date('j'); ?><span class="entry-date__month"><?php echo get_the_date('M'); ?></span></div>
                            <h2 class="entry-title entry-title_spacing ui-title-inner"><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
                        </div>
                        <div class="entry-content">
                            <p><?php echo get_the_excerpt(); ?></p>
                        </div>
                        <div class="entry-footer">
                            <div class="entry-meta">
                            <span class="entry-meta__item">
                                <i class="entry-meta__icon pe-7s-user"></i>by <?php echo get_the_author_link(); ?>
                            </span>
                            <span class="entry-meta__item">
                                <i class="entry-meta__icon pe-7s-comment"></i><a href="<?php echo get_the_permalink() . '#comments'; ?>" class="entry-meta__link"><?php echo get_comments_number(get_the_ID()); ?></a>
                            </span>
                            </div>
                        </div>
                    </div>
                </article>
            <?php } ?>
        </div>
    <?php } elseif ($style == 'B') { ?>
<div data-min480="1" data-min768="2" data-min992="2" data-min1200="3" data-pagination="true"
     data-navigation="false" data-auto-play="4000" data-stop-on-hover="true"
     class="owl-carousel owl-theme enable-owl-carousel">
            <?php while ($the_query->have_posts()) {
                $the_query->the_post(); ?>
                <article class="b-post b-post-5 clearfix">
                    <div class="entry-media">
                        <a href="<?php the_post_thumbnail_url(); ?>" class="js-zoom-images">
                            <img src="<?php the_post_thumbnail_url(); ?>" alt="Foto" class="img-responsive"/>
                        </a>
                    </div>
                    <div class="entry-main">
                        <div class="entry-header">
                            <div class="entry-date"><?php echo get_the_date('j'); ?><span class="entry-date__month"><?php echo get_the_date('M'); ?></span></div>
                            <h2 class="entry-title entry-title_spacing ui-title-inner"><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
                            <div class="entry-meta">
                                <span class="entry-meta__item">
                                    <i class="entry-meta__icon pe-7s-user"></i>by <?php echo get_the_author_link(); ?>
                                </span>
                                <span class="entry-meta__item">
                                    <i class="entry-meta__icon pe-7s-comment"></i><a href="<?php echo get_the_permalink() . '#comments'; ?>" class="entry-meta__link"><?php echo get_comments_number(get_the_ID()); ?></a>
                                </span>
                            </div>
                        </div>
                        <div class="entry-content">
                            <p><?php echo get_the_excerpt(); ?></p>
                        </div>
                    </div>
                </article>
            <?php } ?>
</div>
    <?php }
}
wp_reset_postdata();
?>



    <!-- end post-->

