<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $title
 * @var $description
 * @var $text
 * @var $button_link
 * Shortcode class
 * @var $this WPBakeryShortCode_Section_Award
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);

$button_link = vc_build_link($button_link);
?>


<section class="b-type-c">
    <div class="section__inner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="b-type-c__title"><?php echo $title; ?></h2>
                    <div class="b-type-c__subtitle"><?php echo $description; ?></div>
                    <div class="b-type-c__text">
                        <?php echo $text; ?>
                    </div>
                    <a href="<?php echo $button_link['url']; ?>" class="b-type-c__btn btn btn-default btn-sm btn-effect"><?php echo $button_link['title']; ?></a>
                </div>
            </div>
        </div>
    </div>
</section>