<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $style
 * @var $first_title
 * @var $first_number
 * @var $first_description
 * @var $second_title
 * @var $second_number
 * @var $second_description
 * @var $third_title
 * @var $third_number
 * @var $third_description
 * Shortcode class
 * @var $this WPBakeryShortCode_Section_Progress_List
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts); ?>

<div class="b-progress <?php echo $style == 'A' ? ' wow' : '';?>">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul class="b-progress-list list-unstyled <?php echo $style == 'B' ? 'b-progress-list_mod-a' : '';?>">
                    <li class="b-progress-list__item clearfix">
                        <div class="b-progress-list__label"><?php echo $first_title;?></div>
                        <span data-percent="<?php echo $first_number;?>" class="b-progress-list__percent js-chart">
                            <span class="js-percent"></span>
                        </span>
                        <span class="b-progress-list__name"><?php echo $first_description;?></span>
                    </li>
                    <li class="b-progress-list__item clearfix">
                        <div class="b-progress-list__label"><?php echo $second_title;?></div>
                        <span data-percent="<?php echo $second_number;?>" class="b-progress-list__percent js-chart">
                            <span class="js-percent"></span>
                        </span>
                        <span class="b-progress-list__name"><?php echo $second_description;?></span>
                    </li>
                    <li class="b-progress-list__item clearfix">
                        <div class="b-progress-list__label"><?php echo $third_title;?></div>
                        <span data-percent="<?php echo $third_number;?>" class="b-progress-list__percent js-chart">
                            <span class="js-percent"></span></span>
                        <span class="b-progress-list__name"><?php echo $third_description;?></span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>


