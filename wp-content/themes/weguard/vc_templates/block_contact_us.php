<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $title
 * @var $phone
 * @var $email
 * @var $image
 * @var $style
 * Shortcode class
 * @var $this WPBakeryShortCode_Block_Contact_Us
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);

$img_id = preg_replace('/[^\d]/', '', $image);
$img_meta_array = weguard_wp_get_attachment($img_id);


//if ($style == 'A') { ?>
    <div class="b-contact-banner" style="<?php //echo !empty($img_meta_array['src']) ? 'background-image: url(\'' . $img_meta_array['src'] . '\');' : ''; ?>">
        <div class="border-2-colors">
            <div class="b-contact-banner__inner">
                <h3 class="b-contact-banner__title"><?php echo $title; ?></h3>
                <div class="b-contact-banner__decor ui-decor-type-3 center-block"></div>
                <div class="b-contact-banner__info b-contact-banner__info_lg"><?php echo $phone; ?></div>
                <div class="b-contact-banner__info"><?php echo $email; ?></div>
            </div>
        </div>
    </div>
<?php //} elseif ($style == 'B') { ?>
<!--    <div class="b-contact-banner b-contact-banner_mod-a" style="--><?php //echo !empty($img_meta_array['src']) ? 'background-image: url(\'' . $img_meta_array['src'] . '\');' : ''; ?><!--">-->
<!--        <div class="b-contact-banner__border"></div>-->
<!--        <div class="b-contact-banner__inner">-->
<!--            <h3 class="b-contact-banner__title">--><?php //echo $title; ?><!--</h3>-->
<!--            <div class="b-contact-banner__decor ui-decor-type-3 center-block"></div>-->
<!--            <div class="b-contact-banner__info b-contact-banner__info_lg">--><?php //echo $phone; ?><!--</div>-->
<!--            <div class="b-contact-banner__info">--><?php //echo $email; ?><!--</div>-->
<!--        </div>-->
<!--    </div>-->
<?php //} ?>
