<?php
/**
 *
 * Template part for displaying posts.
 *
 */
?>

<article class="b-post b-post-2 clearfix">
    <div class="entry-media">
        <a href="<?php the_post_thumbnail_url(); ?>" class="js-zoom-images">
            <?php the_post_thumbnail(); ?>
        </a>
    </div>
    <div class="entry-main">
        <div class="entry-header">
            <div class="entry-date"><?php echo get_the_date('j M Y'); ?></div>
            <div class="entry-meta">
                <span class="entry-meta__item">
                    <i class="entry-meta__icon pe-7s-user"></i>By <?php echo get_the_author_link(); ?>
                </span>
                <span class="entry-meta__item">
                    <i class="entry-meta__icon pe-7s-comment"></i>Comments:<a href="<?php echo get_the_permalink() . '#comments'; ?>" class="entry-meta__link"> <?php echo get_comments_number(); ?></a>
                </span>
                <span class="entry-meta__item">
                    <i class="entry-meta__icon pe-7s-portfolio"></i>
                    <?php
                    $categories = wp_get_post_categories(get_the_ID(), array('fields' => 'all'));
                    foreach ($categories as $index => $category) {
                        $delimiter = ($index + 1) == count($categories) ? '' : ', ';
                        echo '<a href="' . get_category_link($category->term_id) . '" class="entry-meta__link">' . $category->name . $delimiter . '</a>';
                    }
                    ?>
                </span>
            </div>
            <h2 class="entry-title entry-title_spacing ui-title-inner"><?php echo get_the_title(); ?></h2>
        </div>
        <div class="entry-content">
            <p><?php echo get_the_excerpt(); ?></p>
        </div>
        <div class="entry-footer">
            <a href="<?php echo get_the_permalink(); ?>" class="btn btn-default btn-sm btn-effect">read more</a>
            <a href="<?php echo get_the_permalink(); ?>" class="entry-footer__link"><i class="icon pe-7s-share"></i> Share This</a>
        </div>
    </div>
</article>
