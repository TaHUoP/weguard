<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package weguard
 */

?>
<footer class="footer">
    <div class="footer__main footer__main_mod-a parallax-bg parallax-dark">
        <ul class="bg-slideshow">
            <li>
                <div style="background-image:url(http://weguard/wp-content/themes/weguard/assets/media/components/footer/bg.jpg)" class="bg-slide"></div>
            </li>
        </ul>
        <div class="parallax__inner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <a href="<?php echo get_home_url(); ?>" class="footer__logo">
                            <img src="<?php echo get_theme_mod('footer_logo'); ?>" alt="Logo" class="img-responsive center-block"/>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="footer-main-section footer-main-section_left">
                            <div class="footer__contact footer__contact_lg"><?php echo get_theme_mod('footer_phone'); ?></div>
                            <div class="footer__contact"><?php echo get_theme_mod('footer_email'); ?></div>
                            <div class="footer__contact"><?php echo get_theme_mod('footer_address'); ?></div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="footer-main-section footer-main-section_right">
                            <div class="footer-form__info">Get the security news, Join us!</div>
                            <?php echo do_shortcode(get_theme_mod('footer_form')); ?>
<!--                            <form class="footer-form">-->
<!--                                <input type="text" placeholder="Enter Your Email ..." class="footer-form__input"/>-->
<!--                                <button class="footer-form__btn"><i class="icon pe-7s-mail"></i></button>-->
<!--                            </form>-->
                        </div>
                    </div>
                </div>
            </div>
            <?php
            $footer_post = get_theme_mod('footer_choice_section');
            // The Query
            $the_query = new WP_Query(array('p' => (int)$footer_post, 'post_type' => 'staticblocks'));

            // The Loop
            if ($the_query->have_posts()) :
                while ($the_query->have_posts()) : $the_query->the_post();
                    the_content();
                endwhile;
                /* Restore original Post Data */
                wp_reset_postdata();
            else :
                // no posts found
            endif;
            ?>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">Copyrights 2016<a href="home.html" class="copyright__link"> We Guard Security Services</a> All rights reserved.<a href="home.html" class="copyright__link-2">Privacy Policy</a><a href="home.html" class="copyright__link-2">Terms & Conditions</a></div>
            </div>
        </div>
    </div>
</footer>
</div>
<!-- end layout-theme-->

<?php wp_footer(); ?>

</body>
</html>


