<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $style
 * @var $slides [N]['title']
 * @var $slides [N]['text']
 * @var $slides [N]['name']
 * @var $slides [N]['company']
 * @var $slides [N]['image']
 * Shortcode class
 * @var $this WPBakeryShortCode_Section_Customers_Slider
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$atts = $this->convertAttributesToArray($atts);
extract($atts);
$slides = (array)vc_param_group_parse_atts($slides);

$button_link = vc_build_link($button_link);
if ($style == 'A') { ?>
    <div data-min480="1" data-min768="2" data-min992="2" data-min1200="3" data-pagination="false"
         data-navigation="false" data-auto-play="4000" data-stop-on-hover="true"
         class="owl-carousel owl-theme enable-owl-carousel">
        <?php foreach ($slides as $slide) {
            $img_id = preg_replace('/[^\d]/', '', $slide['image']);
            $img_meta_array = weguard_wp_get_attachment($img_id);
            ?>
            <article class="b-reviews-1 b-reviews-1_mod-a">
                <h3 class="b-reviews-1__title"><?php echo $slide['title']; ?></h3>
                <blockquote class="blockquote blockquote-2">
                    <p><?php echo $slide['text']; ?></p>
                    <footer class="blockquote__footer">
                        <cite title="Blockquote Title">
                        <span class="blockquote__footer-inner">
                            <span class="blockquote__author"><?php echo $slide['name']; ?></span>
                            <span class="blockquote__category"><?php echo $slide['company']; ?></span>
                        </span>
                        <span class="blockquote__img">
                            <img src="<?php echo $img_meta_array['src']; ?>" alt="<?php echo $img_meta_array['alt']; ?>" class="img-responsive"/>
                        </span>
                        </cite>
                    </footer>
                </blockquote>
            </article>
        <?php } ?>
    </div>
<?php } elseif ($style == 'B') { ?>
    <div data-pagination="true" data-navigation="false" data-single-item="true" data-auto-play="7000"
         data-transition-style="fade" data-main-text-animation="true" data-after-init-delay="3000"
         data-after-move-delay="1000" data-stop-on-hover="true"
         class="owl-carousel owl-theme enable-owl-carousel">
        <?php foreach ($slides as $slide) {
        $img_id = preg_replace('/[^\d]/', '', $slide['image']);
        $img_meta_array = weguard_wp_get_attachment($img_id);
        ?>
        <div class="b-reviews-2 b-reviews-2_mod-a">
            <div class="blockquote__img center-block"><img
                        src="<?php echo $img_meta_array['src']; ?>" alt="<?php echo $img_meta_array['alt']; ?>" class="img-responsive"/>
            </div>
            <blockquote class="blockquote blockquote-3">
                <p><?php echo $slide['text']; ?></p>
                <footer class="blockquote__footer">
                    <cite title="Blockquote Title">
                        <span class="blockquote__author"><?php echo $slide['name']; ?></span>
                        <span class="blockquote__category"><?php echo $slide['company']; ?></span></cite>
                </footer>
            </blockquote>
        </div>
        <?php } ?>
    </div>
<?php } ?>
