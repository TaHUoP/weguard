<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $blue_title
 * @var $main_title
 * @var $text
 * Shortcode class
 * @var $this WPBakeryShortCode_Block_Award
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);

$font_icon = ${'icon_' . $type};


if ($style == 'A') { ?>
    <section class="b-type-a">
    <span class="b-type-a__icon">
         <i class="<?php echo $font_icon; ?>"></i>

        <!--        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="47px" height="50px" viewBox="0 0 47 50">-->
<!--            <path fillRule="evenodd"-->
<!--                  d="M 43.91 12.08C 42.11 14.87 39.65 17.12 36.75 18.66 36.29 25.08 31.32 30.26 25.03 31 25.03 31 25.03 38.1 25.03 38.1 30.44 38.73 34.98 42.65 36.36 48.07 36.36 48.07 36.86 50 36.86 50 36.86 50 10.14 50 10.14 50 10.14 50 10.64 48.07 10.64 48.07 12.02 42.65 16.56 38.73 21.97 38.1 21.97 38.1 21.97 31 21.97 31 15.68 30.26 10.71 25.08 10.25 18.66 7.35 17.12 4.89 14.87 3.09 12.08 1.07 8.95 0 5.3 0 1.55 0 1.55 0-0 0-0 0-0 47-0 47-0 47-0 47 1.55 47 1.55 47 5.3 45.93 8.95 43.91 12.08ZM 10.22 3.09C 10.22 3.09 3.14 3.09 3.14 3.09 3.6 7.94 6.23 12.34 10.22 15.04 10.22 15.04 10.22 3.09 10.22 3.09ZM 14.31 46.91C 14.31 46.91 32.69 46.91 32.69 46.91 31.01 43.41 27.47 41.11 23.5 41.11 19.53 41.11 15.99 43.41 14.31 46.91ZM 33.72 3.09C 33.72 3.09 13.28 3.09 13.28 3.09 13.28 3.09 13.28 17.69 13.28 17.69 13.28 23.37 17.87 27.99 23.5 27.99 29.13 27.99 33.72 23.37 33.72 17.69 33.72 17.69 33.72 3.09 33.72 3.09ZM 36.78 3.09C 36.78 3.09 36.78 15.04 36.78 15.04 40.77 12.34 43.4 7.94 43.86 3.09 43.86 3.09 36.78 3.09 36.78 3.09ZM 27.3 20.48C 27.3 20.48 23.5 18.77 23.5 18.77 23.5 18.77 19.7 20.48 19.7 20.48 19.7 20.48 20.14 16.31 20.14 16.31 20.14 16.31 17.35 13.19 17.35 13.19 17.35 13.19 21.42 12.32 21.42 12.32 21.42 12.32 23.5 8.68 23.5 8.68 23.5 8.68 25.57 12.32 25.57 12.32 25.57 12.32 29.65 13.19 29.65 13.19 29.65 13.19 26.86 16.31 26.86 16.31 26.86 16.31 27.3 20.48 27.3 20.48Z"-->
<!--                  fill="rgb(51,51,51)"></path>-->
<!--        </svg>-->
    </span>
        <div class="b-type-a__inner">
            <h2 class="b-type-a__title"><span class="b-type-a__title_emphasis color-secondary"><?php echo !empty($blue_title) ? $blue_title . ' ' : ''; ?></span><?php echo $main_title; ?></h2>
            <div class="b-type-a__text"><?php echo $text; ?></div>
        </div>
    </section>
<?php } elseif ($style == 'B') { ?>
    <section class="b-type-e">
        <h2 class="b-type-e__title ui-title-inner-1"><?php echo $main_title; ?></h2>
        <div class="b-type-e__text"><?php echo $text; ?></div>
    </section>
<?php } ?>
