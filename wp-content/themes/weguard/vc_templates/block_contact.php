<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $title
 * @var $text
 * @var $type
 * Shortcode class
 * @var $this WPBakeryShortCode_Block_Contact
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);

$font_icon = ${'icon_' . $type}; ?>

<div class="b-contacts"><i class="b-contacts__icon <?php echo $font_icon;?>"></i>
    <div class="b-contacts__name color-primary"><?php echo $title;?></div>
    <div class="b-contacts__contacts"><?php echo $text;?></div>
</div>
