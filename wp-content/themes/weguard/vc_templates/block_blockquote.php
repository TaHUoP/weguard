<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $style
 * @var $title
 * @var $text
 * @var $name
 * @var $position
 * @var $image
 * Shortcode class
 * @var $this WPBakeryShortCode_Block_Blockquote
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);

$img_id = preg_replace('/[^\d]/', '', $image);
$img_meta_array = weguard_wp_get_attachment($img_id);

if ($style == 'A') { ?>
    <article class="b-reviews-1">
        <h3 class="b-reviews-1__title"><?php echo $title; ?></h3>
        <blockquote class="blockquote blockquote-2">
            <p><?php echo $text; ?></p>
            <footer class="blockquote__footer">
                <cite title="Blockquote Title">
                    <span class="blockquote__footer-inner">
                        <span class="blockquote__author"><?php echo $name; ?></span>
                        <span class="blockquote__category"><?php echo $position; ?></span>
                    </span>
                    <span class="blockquote__img">
                        <img src="<?php echo $img_meta_array['src']; ?>" alt="<?php echo $img_meta_array['alt']; ?>" class="img-responsive"/>
                    </span>
                </cite>
            </footer>
        </blockquote>
    </article>
<?php } elseif ($style == 'B') { ?>
    <div class="b-reviews-2 bg-grey">
        <div class="blockquote__img center-block">
            <img src="<?php echo $img_meta_array['src']; ?>" alt="<?php echo $img_meta_array['alt']; ?>" class="img-responsive"/>
        </div>
        <blockquote class="blockquote blockquote-3">
            <p><?php echo $text; ?></p>
            <footer class="blockquote__footer">
                <cite title="Blockquote Title">
                    <span class="blockquote__author"><?php echo $name; ?></span>
                    <span class="blockquote__category"><?php echo $position; ?></span>
                </cite>
            </footer>
        </blockquote>
    </div>
<?php } elseif ($style == 'C') { ?>
    <blockquote class="blockquote blockquote-1">
        <p><?php echo $text; ?></p>
    </blockquote>
<?php } elseif ($style == 'D') { ?>
    <div class="section__inner">
        <div class="b-reviews-3">
            <div class="blockquote__img center-block">
                <img src="<?php echo $img_meta_array['src']; ?>" alt="<?php echo $img_meta_array['alt']; ?>" class="img-responsive"/>
            </div>
            <blockquote class="blockquote blockquote-4">
                <header class="blockquote__header">
                    <cite title="Blockquote Title">
                        <span class="blockquote__author"><?php echo $name; ?></span>
                        <span class="blockquote__category"><?php echo $position; ?></span>
                    </cite>
                </header>
                <p><?php echo $text; ?></p>
            </blockquote>
        </div>
        <!-- end b-reviews-->
    </div>
<?php } ?>

