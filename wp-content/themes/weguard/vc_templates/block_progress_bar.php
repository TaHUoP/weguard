<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $values[N]['label']
 * @var $values[N]['value']
 * Shortcode class
 * @var $this WPBakeryShortCode_Block_Progress_Bar
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$atts = $this->convertAttributesToArray( $atts );
extract($atts);
$values = (array) vc_param_group_parse_atts( $values );
?>
<div class="progress-block-group">
    <?php foreach ($values as $data){?>
    <div class="progress-block">
        <div class="progress__title"><?php echo $data['label']; ?></div>
        <div class="progress progress-2">
            <div role="progressbar" aria-valuenow="90" aria-valuemin="0" arivaluemax="100"
                 style="width: <?php echo $data['value']; ?>%" class="progress-bar bg-primary"><span
                    class="progress-bar__number"><?php echo $data['value']; ?>%</span></div>
        </div>
    </div>
    <?php } ?>
</div>
