<?php
$out = $transName = $cacheTime = $carousel_class = '';

extract(shortcode_atts(array(
	'username'=>'',
	'consumer_key'=>'',
	'consumer_secret'=>'',
	'access_token' => '',
	'access_token_secret' => '',
	'num_of_tweets' => '5',
	'disable_carousel' => '',
	'css_animation' => '',
), $atts));


$transName = 'vc_list_tweets'.$username;
$cacheTime = 20;
$carousel_class = $disable_carousel == 0 ? 'disable_carousel' : 'owl-carousel';

use Abraham\TwitterOAuth\TwitterOAuth;

$out = $css_animation != '' ? '<div class="twitter animated" data-animation="' . esc_attr($css_animation) . '">' : '<div class="twitter">';								

if( !empty($username) && !empty($consumer_key) && !empty($consumer_secret) && !empty($access_token) && !empty($access_token_secret)  ){
	$twitterConnection = new TwitterOAuth( $consumer_key , $consumer_secret , $access_token , $access_token_secret	);		
	if(false === ($twitterData = get_transient($transName) ) ){
		
		$twitterConnection = new TwitterOAuth( $consumer_key , $consumer_secret , $access_token , $access_token_secret	);			
		$twitterData = $twitterConnection->get(
				  'statuses/user_timeline',
				  array(
					'screen_name'     => $username ,
					'count'           => $num_of_tweets
					)
				);
		
		if($twitterConnection->getLastHttpCode() !== 200)
		{
			$twitterData = get_transient($transName);
		}
		// Save our new transient.
		set_transient($transName, $twitterData, 60 * $cacheTime);
	}

	if( !empty($twitterData) && is_array($twitterData)  && !isset($twitterData['error'])){
		$i=0;
		$hyperlinks = true;
		$encode_utf8 = true;
		$twitter_users = true;
		$update = true;
$out .= '
	<article>
		<i class="fa fa-twitter"></i>
		<div id="twitter-carousel" class="'.esc_attr($carousel_class).' owl-theme">';
		foreach($twitterData as $item){
				$msg = $item->text;
				$permalink = esc_url('http://twitter.com/#!/'. $username .'/status/'. $item->id_str);
				if($encode_utf8) $msg = utf8_encode($msg);
					$link = $permalink;
$out .= '
			<div class="twitter-feed">
		';
				if ($hyperlinks) { $msg = $this->hyperlinks($msg); }
				if ($twitter_users) { $msg = $this->twitter_users($msg); }
$out .= '<p>'.wp_kses_post($msg).'</p>';
				if($update) {
					$time = strtotime($item->created_at);
					if ( ( abs( time() - $time) ) < 86400 )
						$h_time = sprintf( __('%s ago', 'PixTheme'), human_time_diff( $time ) );
					else
						$h_time = date(__('Y/m/d', 'PixTheme'), $time);
$out .= sprintf( __('%s', 'twitter-for-wordpress', 'PixTheme'),' <footer><a href="'.esc_url('http://twitter.com/'.$username).'">@'.esc_attr($username).'</a> <cite title="' . date(__('Y/m/d H:i:s', 'PixTheme'), $time) . '">' . wp_kses_post($h_time) . '</cite></footer>' );
				}
$out .= '
			</div>
';
				$i++;
				if ( $i >= $num_of_tweets ) break;
		}
$out .= '
		</div>
	</article>
';
	} else { 
		$out .= __('Sorry , Twitter seems down or responds slowly.', 'PixTheme'); 
	}
}
else{
		$out .= __('You need to Setup Twitter API OAuth settings first', 'PixTheme');
}

$out .= '</div>';

echo $out;